# 进程命名空间增强设计
# LiteOS-A 设计

## 工作原理
进程命名空间增强的实现依赖于`readlink`的实现。在进程命名空间PidContainer添加ContainerBase结构体，结构体中vnum字段记录PidContainer的创建。
当执行ls /proc/[pid]/container/pid 或者ls /proc/[pid]/container/pid_for_children时，先用`VnodeLookup`函数，根据路径取得最后子目录的vnode信息，第二步调用vnode的Readlink操作取得链接信息。

`readlink`详细说明以及实现过程，参见`readlink`章节。

## 数据结构设计

修改进程命名空间PidContainer结构体，在进程命名空间中添加containerBase结构体。

~~~
	typedef struct PidContainer {
		...
    	struct ContainerBase      containerBase;
		...
	} PidContainer;

	struct ContainerBase {
   		UINT32 vnum;
	};
~~~

## 流程设计

 - 初始化和创建PidContainer时，记录pidContainer->containerBase->vnum；
~~~
 ContainerAllocVnum(&(pidContainer->containerBase.vnum));
~~~

- `Readlink`的时候，先调用`VnodeLookup`函数获取vnode信息，再调用`VfsProcfsNSMntReadlink`函数，根据pid或者pid_for_children的vnode信息取得ProcessCB信息，从ProcessCB中取得PidContainer信息，取得pidContainer->containerBase->vnum，格式化到用户态buffer中。

- 进程命名空间增强实现结果示例：
![进程命名空间增强实现结果图](images/进程命名空间增强.png)

## 规格说明
 - 不涉及。

## 编译开关

 - 同进程命名空间功能一致，参见进程命名空间设计章节。