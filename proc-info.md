# 内核信息数据的保存和获取设计

## 1. 需求分析

支持以下内核信息数据保存和获取

| 文件     | 描述     |
| :---- | :---- |
| /proc/meminfo | 系统内存信息 |
| /proc/filesystems | 文件系统信息 |
| /proc/[pid]/meminfo | 进程的内存占用信息 |
| /proc/[pid]/cpup | 进程的CPUP信息 |


## 2 获取系统内存信息

### 2.1 工作原理

内核初始化过程中，创建/proc目录，并在/proc目录下生成`meminfo`文件，且注册操作该文件（读取）的回调函数。用户读取该文件时会从对应的回调函数读取内容。

### 2.2 数据结构设计
  
  内核已有数据结构的定义
  
	typedef struct {
		UINT32 totalUsedSize;
		UINT32 totalFreeSize;
		UINT32 maxFreeNodeSize;
		UINT32 usedNodeNum;
		UINT32 freeNodeNum;
	#ifdef LOSCFG_MEM_WATERLINE
		UINT32 usageWaterLine;
	#endif
	} LOS_MEM_POOL_STATUS;

### 2.3 流程设计

![meminfo 流程](images/proc-meminfo-flow.png)

## 3 获取文件系统数据信息

### 3.1 工作原理

内核初始化过程中，创建/proc目录，并在/proc目录下生成`filesystems`文件，且注册操作该文件（读取）的回调函数。用户读取该文件时会从对应的回调函数读取内容。

### 3.2 数据结构设计

内核已有数据结构的定义

	struct fsmap_t
	{
	  const char                      *fs_filesystemtype;
	  const struct                    MountOps *fs_mops;
	  const BOOL                      is_mtd_support;
	  const BOOL                      is_bdfs;
	};


### 3.3 流程设计

![filesystems 流程](images/proc-filesystems-flow.png)

## 4 获取进程内存占用信息

### 4.1 工作原理

内核初始化过程中，创建/proc目录，并通过`VnodeLookup`找到对应进程的PCB信息，从`PCB`中读取进程的内存信息。

### 4.2 数据结构设计

	//PidEntry结构体变量，pid下子目录列表
	static const struct ProcfsPidEntry g_procfsPidSubEntries[] = {
		...
		PID("meminfo", PID_FILE_MODE, &g_procfsPidMenInfoRead, &PROC_PID_MEMINFO_READ),		
		...
	};
	
	//MemInfo文件的Operations
	static struct ProcFileOperations PROC_PID_MEMINFO_READ = {
		.read = PIDMemInfoProcRead,
	};

	//MemInfo节点的VnodeOps
	static struct VnodeOps g_procfsPidMenInfoRead = {
		.Lookup = ProcfsContainerLookup,
		.Getattr = VfsProcfsStat,
		.Readdir = ProcfsContainerReaddir,
		.Opendir = VfsProcfsOpendir,
		.Closedir = VfsProcfsClosedir,
		.Truncate = VfsProcfsTruncate
	};
### 4.3 流程设计

- 根据路径，完成vnode的查找或者创建
- 在vnode中记录对应PCB信息
- 读取PCB进程memory使用信息

![/proc/[pid]/meminfo 流程](images/proc-pid-meminfo-flow.png)


## 5 获取进程的CPUP信息

### 5.1 工作原理

内核初始化过程中，创建/proc目录，并通过`VnodeLookup`找到对应进程的PCB信息，从`PCB`中读取进程的CPUP信息。

### 5.2 数据结构设计

	//PidEntry结构体变量，pid下子目录列表
	static const struct ProcfsPidEntry g_procfsPidSubEntries[] = {
		...
		PID("cpup", PID_FILE_MODE, &g_procfsPidCpupRead, &PROC_PID_CPUP_READ),
		...
	};

	//CPUP文件的Operations
	static struct ProcFileOperations PROC_PID_CPUP_READ = {
		.read = PIDCpupProcRead,
	};

	//CPUP文件的VnodeOps
	static struct VnodeOps g_procfsPidCpupRead = {
		.Lookup = ProcfsContainerLookup,
		.Getattr = VfsProcfsStat,
		.Readdir = ProcfsContainerReaddir,
		.Opendir = VfsProcfsOpendir,
		.Closedir = VfsProcfsClosedir,
		.Truncate = VfsProcfsTruncate
	};

### 5.3 流程设计
- 根据路径，完成vnode的查找或者创建
- 在vnode中记录对应PCB信息
- 读取PCB进程CPUP使用信息

![/proc/[pid]/meminfo 流程](images/proc-pid-cpup-flow.png)

## 6 规格说明

不涉及

## 7 编译开关

不涉及

