# setns

## 工作原理

setns接口为：int setns(int fd, int type)。

 - 第一个入参fd用于传递文件描述符，指定需要切换的目标容器；
 
  - 第二个入参type用于指明哪一个类型的容器需要切换。

执行setns接口用于将当前进程指定类型的容器切换至fd指定的容器。

![setns 工作原理](images/SysSetns-原理设计.png)

涉及到以下操作：
 1. 根据入参fd查找需要切换的目标容器`Container`，并根据目标容器类型核对入参 flag 的类型是否一致/是否非法
 2. 创建新的`ContainerBundle`，原`ContainerBundle`引用计数`-1`
    - 原有`ContainerBundle`如果引用计数`归0`时，该`ContainerBundle`被释放，其下的所有`Container`引用计数`-1` 
 3. 如果某个`Container`的`flag`没有设置，则该`Container`的指针赋值给新的`ContainerBundle`，引用计数`+1`
 4. 如果某个`Container`的`flag`有设置，则将`ContainerBundle`引用目标容器`Container`，并将目标容器`Container`引用计数递增`+1`
 5. 将准备好的`ContainerBundle`赋值给当前进程PCB，完成 `setns` 的切换操作。
 


## 流程设计

![setns 流程设计](images/SysSetns-流程设计.png)

### 函数设计：
1. `GetTargetContainer`：根据`fd`找到目标容器`targetContainer`
   - `GetVnode`: 将用户fd转换为系统fd，从而找到对应的vnode
2. `GetTargetContainerType`：根据`fd`确定合法的目标容器类型`targetType`
3. `CopyContainerBundleOnly`：根据`flag`生成新的`ContainerBundle`
4. `ReplaceUtsContainerInBundle`：根据`flag`用目标容器替换新的`ContainerBundle`对应容器指针
5. `AssignContainers`：给`PCB`赋值新的`ContainerBundle`，然后对旧的`ContainerBundle`引用计数`-1`

![setns 函数流程设计](images/SysSetns-伪码流程.png)

### 以**主机命名空间**为例，命名空间相关函数设计如下：
1. `HandleUtsContainer`：根据`flag`计算新的`UtsContainer`
2. `GetUtsContainer`：引用计数`+1`
3. `CreateNewUtsContainer`：创建新的命名空间，做**资源拷贝**
4. `DerefUtsContainer`：引用计数`-1`
5. `FreeUtsContainer`：**释放资源**


其他命名空间函数设计参考**主机命名空间**

### 特例：

#### 1. 对于**PID命名空间**的setns处理：

PID命名空间setns进行目标容器设置，不对当前进程生效，而是对当前进程后续创建的子进程、孙子进程生效。

setns成功之后（将目标容器记录在PID的pidContainerForChildren中。

 - 当前进程创建子进程、孙子进程时，总是基于pidContainerForChildren：

   - 如果未指定“CLONE_NEWPID”，则子进程放入到目标容器中。

   - 如果指定了“CLONE_NEWPID”， 则子进程放入到目标容器的下一级容器中。（PID命名空间不可超过3层）

 - 获取和操作当前进程PID时，总是基于 pidContainer。  
 
#### 2. 对于**USER命名空间**的setns处理：

 - 根据入参fd查找需要切换的目标User容器`Container`，并根据目标容器类型核对入参 flag 的类型是否一致/是否非法

 - 创建新的`credentials`，初始化引用计数为1，将PCB原指向的`credentials`**资源拷贝**至新的`credentials`

 - 新的`credentials`中容器指针指向目标user容器`userContainer`，并将该目标user容器引用计数`+1`，将原来指向的 user 容器引用计数`-1` 

 - 如果某个`Container`的`flag`有设置，则将`ContainerBundle`引用目标容器`userContainer`，并将目标容器`userContainer`引用计数递增`+1`

 - 将准备好的`credentials`赋值给当前进程PCB，完成 `setns` 的切换操作。

 - 原`credentials`引用计数`-1`

    - 若原有`credentials`如果引用计数`归0`，该`credentials`被释放，其下的user容器`userContainer`引用计数`-1` 

## 其他说明

 sentns的入参fd通过`open`打开文件获取，打开文件时，`/proc/[pid]/continer/` 下的各容器文件将被视作普通文件，即将原始文件名作为open对象，而不是将其链接目标作为open对象。
 
 原因是procfs下各容器文件虽属于链接文件类型，但是其链接目标并不是有效文件，而是容器vnum数值。
 
 比如在 open "/proc/10/container/uts" 时，该链接文件指向目标为 `uts:[4026531841]`，并不指向有效的文件，所以不应该对链接目标进行文件访问，而应该访问原始文件名。

 ![setns open文件说明](images/SysSetns_fd_open_proc.png)
 
## 规格说明

setns每次只能切换一个容器。

## 编译开关

~~~
编译宏定义：
#ifdef LOSCFG_UTS_CONTAINER ------各容器编译宏
#ifdef LOSCFG_MOUNT_CONTAINER
#ifdef LOSCFG_PID_CONTAINER
#ifdef LOSCFG_NET_CONTAINER
#ifdef LOSCFG_USER_CONTAINER
#ifdef LOSCFG_CONTAINER // ----容器功能总编译宏
#ifdef LOSCFG_PROCESS_LIMITS // --PLIMIT编译宏
~~~
参考“命名空间详细设计”
