# unshare

## 工作原理

![unshare 工作原理](images/posix-api-unshare-theory.drawio.png)

涉及到以下操作：

1. 创建新的`ContainerBundle`，原`ContainerBundle`引用计数`-1`
2. 如果某个`Container`的`flag`没有设置，则该`Container`的指针赋值给新的`ContainerBundle`，引用计数`+1`
3. 如果某个`Container`的`flag`有设置，则该`Container`做**资源拷贝**，并赋值；引用计数初始化为`1`
4. 原有`ContainerBundle`如果引用计数`归0`时，该`ContainerBundle`被释放，其下的所有`Container`引用计数`-1`
5. 原有`Container`如果引用计数`归0`时，该`Container`被释放

## 流程设计

![unshare 流程设计](images/posix-api-unshare-flow.drawio.png)

函数设计：

1. `UnshareContainers`：根据`flag`生成新的`ContainerBundle`
2. `AssignContainers`：给`PCB`赋值新的`ContainerBundle`，然后对旧的`ContainerBundle`引用计数`-1`

以**主机命名空间**为例，命名空间相关函数设计如下：
1. `HandleUtsContainer`：根据`flag`计算新的`UtsContainer`
2. `GetUtsContainer`：引用计数`+1`
3. `CreateNewUtsContainer`：创建新的命名空间，做**资源拷贝**
4. `DerefUtsContainer`：引用计数`-1`
5. `FreeUtsContainer`：**释放资源**

其他命名空间函数设计参考**主机命名空间**

## 规格说明

不涉及

## 编译开关

~~~
编译宏定义：
#ifdef LOSCFG_UTS_CONTAINER ------各容器编译宏
#ifdef LOSCFG_MOUNT_CONTAINER
#ifdef LOSCFG_PID_CONTAINER
#ifdef LOSCFG_NET_CONTAINER
#ifdef LOSCFG_USER_CONTAINER
#ifdef LOSCFG_CONTAINER // ----容器功能总编译宏
#ifdef LOSCFG_PROCESS_LIMITS // --PLIMIT编译宏
~~~
参考“命名空间详细设计”
