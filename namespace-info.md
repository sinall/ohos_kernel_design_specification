# 1. Linux5.10调查
## 工作原理

	# ls -l /proc/33/ns/mnt
	lrwxrwxrwx 1 root root 0 May 18 16:44 mnt -> 'mnt:[3]'

ls命令可以取得进程下命名空间信息和进程关系，ls实现调用opendir和readdir系统调用，ls -l取得文件详细信息，一般通过stat系统调用。
所以针对这几个系统调用涉及的节点操作做修正。

## 数据结构和流程

![linux调查](images/profs_getInfo-linux5.10.drawio.png)


# 2. liteos_a设计

## 数据结构设计

```
/* 以下为追加 */

//pid节点的Operations
static struct VnodeOps procfsPidBaseVops = {
	.Lookup = VfsProcfsPidBaseLookup,
};

//ns节点的Operations
static struct VnodeOps procfsNSDirVops= {
    .Lookup = VfsProcfsNSDirLookup,
    .Readdir = VfsProcfsNSDirReaddir,
};

//pid下子目录的结构，例如ns,fd等
struct ProcPidEntry {
	const char *name;
	unsigned int len;
	mode_t mode;
	const struct VnodeOps *vop;
	const struct file_operations_vfs *fop;
};

//PidEntry结构体变量，pid下子目录列表，包含Vnode操作和file操作
static const struct ProcPidEntry procPidBaseEntries[] = {
{
    "ns", sizeof("ns"), S_IRUSR|S_IXUGO, procfsNSDirVops, {}},
    "fd", sizeof("fd"), S_IRUSR|S_IXUGO, procfsFdDirVops, {}},
    ...
}

//ns下子目录列表，取得ns目录下内容用
static const struct ProcPidEntry procPidNSEntries[] = {
{
    "mnt", sizeof("mnt"), S_IFLNK | S_IRWXUGO, procfsNSMntVops, {}},
    "pid", sizeof("pid"), S_IFLNK | S_IRWXUGO, procfsNSPidVops, {}},
    "user", sizeof("user"), S_IFLNK | S_IRWXUGO, procfsNSUserVops, {}},
    "uts", sizeof("uts"), S_IFLNK | S_IRWXUGO, procfsNSUtsVops, {}},
    "net", sizeof("net"), S_IFLNK | S_IRWXUGO, procfsNSNetVops, {}},
}

//ns下，mnt节点的Operations
static struct VnodeOps procfsNSMntVops= {
    .Readlink = VfsProcfsNSMntReadlink,
    .Getattr = VfsProcfsNSMntStat,
};
```

## 流程设计
- opendir相关

以下流程图可以看出，opendir最终调用VnodeLookup，因为VnodeLookup中调用各节点的Lookup函数（(*currentVnode)->vop->Lookup），
所以需要对/proc/pid下各节点的Lookup操作做实现。

![opendir](images/profs_getInfo_liteos-a_1.png)

以下pid节点和ns节点的Lookup操作实现在readlink设计中已经说明，详细请参照readlink设计。
```
//pid节点的Operations
static struct VnodeOps procfsPidBaseVops = {
	.Lookup = VfsProcfsPidBaseLookup,
};

//ns节点的Operations
static struct VnodeOps procfsNSDirVops= {
	.Lookup = VfsProcfsNSDirLookup,
};
```

- readdir相关

以下流程图可以看出，readdir最终调用节点的Readdir操作。

![opendir](images/profs_getInfo_liteos-a_2.png)

针对ns节点的VnodeOps，需要实现Readdir操作。加上opendir时需要实现的Lookup，ns节点操作如下。
```
static struct VnodeOps procfsNSDirVops= {
    .Lookup = VfsProcfsNSDirLookup,
    .Readdir = VfsProcfsNSDirReaddir,
};
```

VfsProcfsNSDirReaddir中，循环检查procPidNSEntries中的name是否已经有了ProDirEntry实例，没有则创建他。
然后循环取得procPidNSEntries->name，设置到dir->fd_dir[i].d_name中，作为readdir结果。
```
int VfsProcfsNSDirReaddir(struct Vnode *node, struct fs_dirent_s *dir){
	...
	for (int i = 0; i < ARRAY_SIZE(procPidNSEntries); i++) {
		struct ProcDirEntry *sub = node->data->subdir;
		while (1){
			if (sub == NULL) {
				pn = ProcCreateData(name, mode, node->data，procFileOps, node->data->name);	
			}
			if (ProcMatch(strlen(name), name, sub)) {
				break;
			}
			sub = sub->next;
		}
	}
	
	while(i < ARRAY_SIZE(procPidNSEntries)){
		strncpy_s(dir->fd_dir[i].d_name, dst_name_size, procPidNSEntries[i]->name, strlen());
		dir->fd_dir[i].d_name[dst_name_size - 1] = '\0';
		dir->fd_position++;
		dir->fd_dir[i].d_off = dir->fd_position;
		dir->fd_dir[i].d_reclen = (uint16_t)sizeof(struct dirent);
		
		i++;
	}
	...
}
```


- stat相关

以下流程图可以看出，stat最终调用节点的Getattr操作取得文件属性。

![opendir](images/profs_getInfo_liteos-a_3.png)

针对命名空间ns下文件，需要实现各文件节点的Getattr操作。以mnt为例，实现如下。
```
static struct VnodeOps procfsNSMntVops= {
    .Readlink = VfsProcfsNSMntReadlink,
    .Getattr = VfsProcfsNSMntStat,
};
```

VfsProcfsNSMntStat中，设置文件属性到struct stat中。
```
int VfsProcfsNSMntStat(struct Vnode *node, struct stat *buf)
{
    ...
    buf->st_mode = node->mode;
    buf->st_nlink = ;
    buf->st_uid = node->uid;
    buf->st_gid = node->gid;
    buf->st_mtim = ;
    ...
}
```

- readlink相关

命名空间部分的readlink可以参照[readlink设计.md]中设计。主要取得当前进程的命名空间inum，然后格式化返回，即为readlink结果。
```
VfsProcfsNSReadlink(struct Vnode vnode, char *buf, size_t bufsize)
{
	...
	processCB = OS_PCB_FROM_PID(pid);
	struct nsCommon *ns = ns_ops->get(ProcessCB);
	snprintf(buf, size, "%s:[%u]", vnode->data->name, ns->inum)
	...
}
```

# 3. 规格说明

不涉及

# 4. 编译开关

不涉及
