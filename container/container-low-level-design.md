# 命名空间详细设计

## 名词说明：

 - 需求中的“命名空间”建议英文命名“container（容器）”。

 - 需求中的“容器配额”建议英文命名“ProcessLimits”。

本文中会同时使用这些叫法，不做区别，待名称确定后统一替换。

# 命名空间工作原理

## （一）总体工作机制

一个进程创建子进程时，可以指定将子进程放置到一个新的命名空间中，从而让子进程感知不到其他命名空间的存在，认为自己是在一个独立的系统环境中运行。实现机制是子进程进程控制块“ProcessCB”中的容器集合“ContainerBundle”，包含的命名空间指针分别指明对应的命名空间。当用户态进程访问对应命名空间资源时，访问的是该命名空间中的值，而不是系统root命名空间的值。

如果一个进程创建子进程时，未要求将子进程放置到新的命名空间，则子进程与父进程属于同一个命名空间。实现机制是子进程进程控制块“ProcessCB”中的容器集合“ContainerBundle”，包含的命名空间指针指向父进程所指向的命名空间对象。

PID命名空间和User命名空间分别均需要采用树形结构进行管理，其他命名空间无需采用树形结构管理。

命名空间中最后一个进程退出后，该命名空间占用的资源被销毁，包括使用的锁进行释放、命名空间节点内存释放。


## （二）PID命名空间工作机制

除了系统默认的PID命名空间（PID根命名空间）外，所有的PID命名空间都有一个父命名空间，每个PID命名空间都可以有零到多个子命名空间。 当在一个进程中调用unshare或者clone创建新的PID命名空间时，当前进程原来所在的命名空间为父命名空间，新的命名空间为子命名空间。

在不同的PID命名空间中，同样一个进程的PID可以不一样，换句话说，一个进程在PID根命名空间中是普通进程，在子命名空间中可以是1号进程。

PID根命名空间中，可以访问下层命名空间各进程实际PID编号，下层命名空间使用的进程PID编号为“虚拟PID编号”。

PID命名空间，root命名空间下的各个命名空间存在继承关系。即某一个命名空间是从属于某一个命名空间的。

PID命名空间可以嵌套（目前LiteOS内核规格为最多3层），分别为0层（根层）、1层和2层。

## （三）命名空间规则

1. 其他命名空间没有分层概念、没有继承概念。

2. 命名空间中最后一个进程退出后，该命名空间应该销毁。

3. 内核启动时，生成（所有类型的）根命名空间（root 命名空间），系统的1号进程“UserInit”存在于这个命名空间。

4. 通过clone创建命名空间时，采用符合POSIX标准的FLAG。名称和含义如下：

  - CLONE_NEWUTS，为子进程创建 utsname 命名空间

  - CLONE_NEWIPC，为子进程创建 IPC命名空间

  - CLONE_NEWUSER，为子进程创建 user 命名空间

  - CLONE_NEWPID，为子进程创建 pid 命名空间

  - CLONE_NEWNET，为子进程创建 network 命名空间

  - 如果clone时不指定上述任何FLAG，则子进程与父进程共享全部的命名空间。

5. 命名空间通过setns接口完成从当前命名空间切换到另一个已有的命名空间。

 切换之前，原命名空间的引用计数需要递减。

6. 命名空间通过unshare接口完成从当前命名空间切换到另一个新创建的命名空间。

 切换之前，原命名空间的引用计数需要递减。

# 设计与实现

## （一）详细设计

### 1、容器集合设计

内核PCB进程控制块结构体“ProcessCB”中增加一个容器合集指针，这个指针用于说明进程的命名空间，它指向一个命名空间结合结构体对象“ContainerBundle”。

**数据结构设计**

~~~

ContainerBundle数据结构的构成：
typedef struct ContainerBundle {
    atomic_t count;
    struct UtsContainer *utsContainer;
    struct PidContainer *PidContainer;
    struct NetContainer *netContainer;
    struct MountContainer *mountContainer;
} ContainerBundle;

struct ContainerBase {
    UINT32 vnum;
};

UTS容器数据结构的构成
typedef struct UtsContainer {
    atomic_t count;
    struct utsname utsName;
    struct ContainerBase containerBase;
} UtsContainer;


PID 容器数据结构的构成
typedef struct TagPidContainer {
    atomic_t refCount;
    UINT32 level;
    struct TagPidContainer *parent;
    VpidBase vpidArray[LOSCFG_BASE_CORE_PROCESS_LIMIT];

} PidContainer;


~~~

数据结构的组成示意图如下。

各命名空间包含一个公共结构“ContainerBase”，成员vnum用于存储procfs文件系统表示各容器节点的全局唯一编号。

![容器节点公共定义部分ContainerBase](../images/containerBase.drawio.png)




#### 命名空间生命周期

##### 通过clone接口创建子进程

通过clone接口创建子进程时，要检查FLAG。

 - 如果FLAG指明不需要为子进程新建任何命名空间，则子进程直接复用父进程的ContainerBundle，让子进程 ContainerBundle 指针直接指向父进程ContainerBundle指针指向的对象，并将该ContainerBundle对象引用计数递增（此处要用原子操作）。

伪代码操作方法是：

~~~
childProcessCB->containerBundle = runProcessCB->containerBundle;

atomic_inc(runProcessCB->containerBundle);
~~~

 - 如果FLAG指明要为子进程新建若干个命名空间，则步骤如下：

1. 新建一个containerBundle 节点（malloc申请内存），并将子进程ContainerBundle 指针指向该节点。

2. 然后，新建指定类型的命名空间节点，并进行初始化和复制。然后将ContainerBundle 节点中对应类型的容器指针指向这个新建的容器节点，并初始化该容器的引用计数为1（此处要用原子操作）。

3. 对于其他类型、不需要新建的容器，子进程仍然复用父进程的容器，办法是让子进程的ContainerBundle 节点中该类型容器的指针指向父进程该类型容器，并将父进程该类型容器的引用计数递增。

![命名空间创建过程](../images/PID容器详细设计-创建过程-方案2.drawio.png)


##### 切换命名空间

在unshare接口中指定入参“int flags”，指明将某一类型命名空间后，则将去除对当前进程的该类型命名空间的引用，并新建一个该类型的命名空间，然后再将进程切换到该命名空间中，并递增该命名空间的引用。

通过setns接口可以切换到新的命名空间。


![命名空间变化过程](../images/内存销毁-clone-unshare-setns.drawio.png)


##### 进程退出

进程退出时，首先将本进程PCB中的 ContainerBundle 引用计数取消引用（递减1），判断：

1. 如果ContainerBundle 引用计数递减后变为0，则需要：

 - （1）将ContainerBundle 所指向的每个容器引用计数也取消引用（递减1）。如果某个容器引用计数为0，则需要将该容器销毁。

 - （2）由于本进程的ContainerBundle 引用计数为0，则将该ContainerBundle进行销毁。

![进程退出-1](../images/内存销毁-进程退出-1.drawio.png)


2. 如果ContainerBundle 引用计数递减后不是0，则：

 - 对ContainerBundle 所指向的每个容器引用计数不需要取消引用。

![进程退出-2](../images/内存销毁-进程退出2.drawio.png)

# 相关技术要求/规范

## 命名空间特性的关闭与打开

采用编译宏、编译脚本中参数结合，完成特性的开关控制。

~~~
编译宏定义：
#ifdef LOSCFG_UTS_CONTAINER ------各容器编译宏
#ifdef LOSCFG_MOUNT_CONTAINER
#ifdef LOSCFG_PID_CONTAINER
#ifdef LOSCFG_NET_CONTAINER
#ifdef LOSCFG_USER_CONTAINER
#ifdef LOSCFG_CONTAINER // ----容器功能总编译宏
#ifdef LOSCFG_PROCESS_LIMITS // --PLIMIT编译宏
~~~

## 命名空间中的孤儿进程回收。

在命名空间中的孤儿进程，均统一由系统1号进程“UserInit”回收。

## root命名空间机制

系统定义一个root命名空间 init Container，在系统启动时作为第一个命名空间存在，UserInit 进程就存在于该命名空间。

其他命名空间的创建，均在root命名空间创建之后。

## 访问时的读写锁。

在进程内操作容器数据时，如果无法采用原子操作，则需要添加互斥锁，进行保护。

# DFx 设计

  - [DFx设计](container-DFx-design.md)



