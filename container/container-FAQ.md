# FAQ

# Kernel 5.10 调查对比
## 相关技术点调查

1、命名空间特性的关闭与打开

某个类型命名空间特性关闭与打开，Linux Kernel 5.10采用了编译宏控制。
举例来说，PID命名空间对应编译宏“CONFIG_PID_NS”。有两种情况：
?如果开启该编译宏，则父进程为子进程创建PID命名空间时，会完成新的命名空间创建。
?如果关闭该编译宏，则父进程为子进程创建PID命名空间时，直接共享/复用父进程的命名空间（也就是系统始终只存在同一个命名空间----root命名空间）
PS：但是无论编译宏开启还是关闭，全局变量“init_nsproxy”是始终会定义的（root 命名空间），PCB结构体“task_struct”中始终会包含一个指针“nsproxy”（该进程的命名空间）。

2、命名空间中的孤儿进程回收。

在命名空间中的孤儿进程，都由该命名空间中的1号进程回收，不再由系统1号进程“init”回收。
对应代码：
/* 命名空间中子进程的回收函数“reaper”总是设置为该命名空间中的 1号进程 */
static inline bool is_child_reaper(struct pid *pid)
{
	return pid->numbers[pid->level].nr == 1;
}

3、root命名空间机制
Linux Kernel中定义了一个root命名空间“init_nsproxy”，在系统启动时作为第一个命名空间存在，init进程就存在于该命名空间。

4、访问时的读写锁。
在访问命名空间变量时，加锁保护。down_read(&uts_sem);

## 设计与实现
1、数据结构
（1）root 命名空间全局变量：“struct nsproxy init_nsproxy” 

（2）命名空间集合结构体：“struct nsproxy” 
