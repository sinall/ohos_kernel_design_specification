# 开发指南

## 1. 系统维测

### 容器隔离

 - 容器隔离之后，系统用户可使用'ls'命令访问 /proce/[pid]/container/ 目录进行查看和确认。

![容器维测](../images/进程命名空间增强.png)

 - chroot 切换当前进程根目录后，系统用户可使用'ls'命令访问 /proce/[pid]/root/ 目录进行查看和确认。
 
 ![chroot维测](../images/chroot_dfx.png)
 
### 容器配额

/proc/plimits 目录作为容器配额根目录，其下的所有文件只读，不能写（所有文件的值，为系统的最大值，不支持写），权限如下：

![method](../images/plimits_liteos_filemode_root.png)


系统用户对容器配额进行配置之后，在 /proc/plimits/[test] 生成对应子目录，该类目录下文件全部可读，部分可写
（plimits.controller_add、plimits.controller_delete、plimits.procs、pids.max、sched.period、sched.quota、memory.limit）：

![method](../images/plimits_liteos_filemode_default.png)

## 2. 代码实例

```
（1）实例. 创建UTS容器

static int childFun(void *p)
{
    pid_t pid = getpid();
    sleep(2);
    return 3;
}

int testFreeMemory(void *ptr)
{
    if (ptr)
    {
        free(ptr);
    }
    return 0;
}

UINT32 TestUts(VOID)
{
    int childPid;
    void *pstk;
    int ret;
    int status;

    pstk = malloc(STACK_SIZE);
    if (pstk == NULL)
    {
        return -1;
    }

    childPid = clone(childFun, (char *)pstk + STACK_SIZE, CLONE_NEWUTS | SIGCHLD, NULL);
    if (childPid < 0)
    {
        printf("clone failed\n");
        testFreeMemory(pstk);
        return -1;
    }

    ret = waitpid(childPid, &status, 0);
    if (ret < 0)
    {
        printf("wait pid failed\n");
        testFreeMemory(pstk);
        return -1;
    }
    int exitCode = WEXITSTATUS(status);
    if (exitCode != 3)
    {
        printf("child return value invalid\n");
        ret = -1;
    }

    testFreeMemory(pstk);
    return ret;
}
```


```
（2）实例. Unshare 切换当前进程的UTS容器至一个新容器。

static UINT32 TestUnshare(VOID)
{
    int ret;

    ret = unshare(CLONE_NEWNS);

    if (ret == -1)
    {
        printf("unshare failed\n");
        return -1;
    }
    return 0;
}

```

```
（3）实例. setns切换，将当前进程的UTS容器切换至子进程的UTS容器。

static int childFun(void *p)
{
    pid_t pid = getpid();
    sleep(2);
    return 3;
}

int testFreeMemory(void *ptr)
{
    if (ptr)
    {
        free(ptr);
    }
    return 0;
}

static UINT32 TestSetns(VOID)
{
    int childPid;
    void *pstk;
    int fd;
    int ret;
    int status;
    int setFlag;
    char targetpath[100];

    pstk = malloc(STACK_SIZE);
    if (pstk == NULL)
    {
        return -1;
    }

    childPid = clone(childFun, (char *)pstk + STACK_SIZE, CLONE_NEWUTS | SIGCHLD, NULL);

    sprintf(targetpath, "/proc/%d/container/uts", childPid);
    fd = open(targetpath, O_RDONLY | O_CLOEXEC);
    if (fd < 0)
    {
        testFreeMemory(pstk);
        return -1;
    }

    setFlag = CLONE_NEWUTS;
    ret = setns(fd, setFlag);
    if (ret < 0)
    {
        printf("setns failed\n");
    }

    ret = close(fd);
    if (ret < 0)
    {
        printf("close fd failed\n");
        testFreeMemory(pstk);
        return -1;
    }

    ret = waitpid(childPid, &status, 0);
    if (ret < 0)
    {
        printf("wait pid failed\n");
        testFreeMemory(pstk);
        return -1;
    }

    int exitCode = WEXITSTATUS(status);
    if (exitCode != 3)
    {
        printf("child return value invalid\n");
        ret = -1;
    }
    testFreeMemory(pstk);
    return ret;
}

```
