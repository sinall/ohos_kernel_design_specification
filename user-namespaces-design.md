# 用户命名空间设计

# Kernel-5.10 调查

## 简介

namespace提供一种隔离机制，让不同的namespace下的进程看到的全局资源不同，每一个namespace有一个自己独立的全局资源实例

![用户命名空间01](images/user-namespaces-design_01.png)

User namespace是用来隔离和分割管理权限的，管理权限实质分为两部分uid/gid和capability

## user namespace创建

**原理介绍**

操作namespace的相关系统调用有3个：

clone(): 创建一个新的进程并把他放到新的namespace中。

setns(): 将当前进程加入到已有的namespace中。

unshare(): 使当前进程退出指定类型的namespace，并加入到新创建的namespace（相当于创建并加入新的namespace）

user命名空间有层次关系，如下图：

 ![用户命名空间02](images/user-namespaces-design_02.png)

如上图关于user命名空间总结如下:

1. user namespace使用父子关系组成树形结构，进程需要指定树中的某个节点为本进程所在的user namespace

2. user namespace可以嵌套，内核控制最多32层。除了系统默认的user namespace外，所有的user namespace都有一个父user namespace。

3. 当在一个进程中调用unshare或clone创建新的user namespace时，当前进程原来所在的user namespace为父，新的为子

4. 在不同的user namespace中，同一个用户的user id 和 group id 可以不一样。

5. 一个用户可以在父user namespace中是普通用户，而在子中是超级用户。

6. 从Linux 3.8 开始，创建新的user namespace不再需要root权限。

**数据结构**

 ![用户命名空间03](images/user-namespaces-design_03.png)
 
## uid/gid隔离

**原理介绍**

user namespace允许namespace间可以映射用户和用户组ID，这意味着一个进程在namespace里面的用户和用户组ID可以与namespace外面的用户和用户组ID不同，也就是说每个user namespace拥有独立的从0开始的uid/gid。值得一提的是，一个普通进程(namespace外面的用户ID非0)在namespace里面的用户和用户组ID可以为0，换句话说这个普通进程在namespace里面可以拥有root特权的权限。

user 命名空间的uid/gid的映射关系图

 ![用户命名空间04](images/user-namespaces-design_04.png)
 
如上图关于user命名空间uid/gid的映射关系总结如下:

映射namespace层级的uid和gid，是通过往/proc/pid/uid_map和/proc/pid/gid_map写入映射信息实现的。

在这两个文件中，映射信息的格式如下:

ID-inside-ns   ID-outside-ns   length

其中ID-inside-ns和length决定了映射的范围，而ID_outside_ns则需要根据如下两种情况：

1. 如果打开uid_map/gid_map的进程和目标进程在同一个Namespace，则这里的ID-outside-ns应该为parent namespace中的一个uid/gid。

2. 如果打开uid_map/gid_map的进程和目标进程不在同一个namespace，则这里的ID-outside-ns则为该进程所在user namespace中的一个uid/gid。

除了上述的格式要求之外，对于uid/gid的映射还有几点约束：

1. 写入uid_map/gid_map的进程，必须对PID进程所属user namespace拥有CAP_SETUID/CAP_SETGID权限。

2. 写入uid_map/gid_map的进程，必须位于PID进程的parent或者child user namespace。

3. 写入进程在parent namespace中的有效uid/gid。此条规则允许child namespace中的进程为自己设置uid/gid

4. 进程在parent namespace拥有CAP_SETUID/CAP_SETGID权限，那么它将可以映射到parent namespace中的任一uid/gid。此条规则仅用于，具有相应权限的parent namespace中的进程，来映射同namespace内的任一uid/gid。

**数据结构**

 ![用户命名空间05](images/user-namespaces-design_05.png)

## capability隔离

**原理介绍**

在linux内核2.2之后引入了capabilities机制，来对root权限进行更加细粒度的划分。如果进程不是特权进程，而且也没有root的有效id，系统就会去检查进程的capabilities，来确认该进程是否有执行特权操作的的权限。

***capabilities的类型***

capabilities的类型如下表：
 
|  capabilities名称   | 描述  |
|  ----  | ----  |
| CAP_AUDIT_CONTROL  | 启用和禁用内核审计；改变审计过滤规则；检索审计状态和过滤规则 |
| CAP_AUDIT_READ  | 允许通过 multicast netlink 套接字读取审计日志 |
| CAP_AUDIT_WRITE  | 将记录写入内核审计日志 |
| CAP_BLOCK_SUSPEND  | 使用可以阻止系统挂起的特性 |
| CAP_CHOWN  | 修改文件所有者的权限 |
| CAP_DAC_OVERRIDE  | 忽略文件的 DAC 访问限制 |
| CAP_DAC_READ_SEARCH  | 忽略文件读及目录搜索的 DAC 访问限制 |
| CAP_FOWNER  | 忽略文件属主 ID 必须和进程用户 ID 相匹配的限制 |
| CAP_FSETID  | 允许设置文件的 setuid 位 |
| CAP_IPC_LOCK  | 允许锁定共享内存片段 |
| CAP_IPC_OWNER  | 忽略 IPC 所有权检查 |
| CAP_KILL  | 允许对不属于自己的进程发送信号 |
| CAP_LEASE  | 允许修改文件锁的 FL_LEASE 标志 |
| CAP_LINUX_IMMUTABLE  | 允许修改文件的 IMMUTABLE 和 APPEND 属性标志 |
| CAP_MAC_ADMIN  | 允许 MAC 配置或状态更改 |
| CAP_MAC_OVERRIDE  | 覆盖 MAC(Mandatory Access Control) |
| CAP_MKNOD  | 允许使用 mknod() 系统调用 |
| CAP_NET_ADMIN  | 允许执行网络管理任务 |
| CAP_NET_BIND_SERVICE  | 允许绑定到小于 1024 的端口 |
| CAP_NET_BROADCAST  | 允许网络广播和多播访问 |
| CAP_NET_RAW  | 允许使用原始套接字 |
| CAP_SETGID  | 允许改变进程的 GID |
| CAP_SETFCAP  | 允许为文件设置任意的 capabilities |
| CAP_SETPCAP  | 参考 capabilities man page |
| CAP_SETUID  | 允许改变进程的 UID |
| CAP_SYS_ADMIN  | 允许执行系统管理任务，如加载或卸载文件系统、设置磁盘配额等 |
| CAP_SYS_BOOT  | 允许重新启动系统 |
| CAP_SYS_CHROOT  | 允许使用 chroot() 系统调用 |
| CAP_SYS_MODULE  | 允许插入和删除内核模块 |
| CAP_SYS_NICE  | 允许提升优先级及设置其他进程的优先级 |
| CAP_SYS_PACCT  | 允许执行进程的 BSD 式审计 |
| CAP_SYS_PTRACE  | 允许跟踪任何进程 |
| CAP_SYS_RAWIO  | 允许直接访问 /devport、/dev/mem、/dev/kmem 及原始块设备 |
| CAP_SYS_RESOURCE  | 忽略资源限制 |
| CAP_SYS_TIME  | 允许改变系统时钟 |
| CAP_SYS_TTY_CONFIG  | 允许配置 TTY 设备 |
| CAP_SYSLOG  | 允许使用 syslog() 系统调用 |
| CAP_WAKE_ALARM  | 允许触发一些能唤醒系统的东西(比如 CLOCK_BOOTTIME_ALARM 计时器) |

***capabilities的机制***

user namespace的第二个作用是隔离capability。

**数据结构**


# LiteOS-A 设计

user container是用来隔离和分割管理权限的，管理权限实质分为两部分uid/gid和capability。

## uid/gid 隔离

### 工作原理

下图为user container uid/gid隔离原理图：

 ![用户命名空间06](images/user-containers-design_06.png)

 user container的第一个作用是隔离uid/gid，每个user container拥有独立的从0开始的uid/gid。这样容器中的进程可以拥有root权限，但是它的root权限会被限制在一小块范围之内。

 user container需要手工创建父子user container uid/gid的映射关系表uid_map/gid_map实现uid/gid的隔离。

### 数据结构设计

下图为user container的数据结构设计图：

 ![用户命名空间07](images/user-containers-design_07.png)

~~~
 修改进程PCB结构体，添加Credentials的指针。
 typedef struct ProcessCB {
     CHAR processName[OS_PCB_NAME_LEN];
     UINT32 processID;
     UINT16 processStatus;
     ...
     struct rlimit *resourceLimit;
     struct Credentials *credentials;
     struct ContainerBundle *containerBundle;
 } LosProcessCB;

 存储凭据的结构，其中包括全局的uid、gid以及用户命名空间。
 typedef struct Credentials {
     atomic_t count;
     UINT32 uid;
     UINT32 gid;
     UINT32 euid;
     UINT32 egid;
     struct UserContainer *userContainer;
 } Credentials;

 用户命名空间结构，包括命名空间的计数count，层级level，uid、gid的映射关系uidMap、gidMap等。
 typedef struct UserContainer {
     atomic_t count;
     int level;
     UINT32 owner;
     UINT32 group;
     struct UserContainer *parent;
     struct ContainerBase containerBase;
     UidGidMap uidMap;
     UidGidMap gidMap;
 } UserContainer;

 存储uid、gid的映射关系结构，其中包括映射关系的个数extentCount，存储映射关系的数组extent。
 typedef struct UidGidMap {
     UINT32 extentCount;
     union {
         UidGidExtent extent[UID_GID_MAP_MAX_EXTENTS];
     };
 } UidGidMap;

 一组映射关系的结构，其中first表示在子命名空间中的起始id，lowerFirst表示在父命名空间中的起始id，count表示映射的个数。
 typedef struct UidGidExtent {
     UINT32 first;
     UINT32 lowerFirst;
     UINT32 count;
 } UidGidExtent;

~~~

### 流程设计

初始化第一个进程时需要新增Credentials的初始化，流程如下图：

 ![用户命名空间08](images/user-containers-design_08.png)

clone时生成新的用户命名空间，流程如下图：

 ![用户命名空间09](images/user-containers-design_09.png)
 
 uid_map/gid_map存储uid/gid的映射关系表

## capability 隔离

### 工作原理

user container通过设置capability来实现能力隔离。

每个进程在进程初始化的时候调用OsInitCapability对权限进行初始化，对用户提供修改和获取(SysCapSet/SysCapGet)权限的系统调用，用于修改进程的权限。

### 数据结构设计
 
 capabilities的类型如下表：

|  capabilities名称   | 描述  |
|  ----  | ----  |
| CAP_CHOWN  | 修改文件所有者的权限 |
| CAP_DAC_EXECUTE  | 忽略文件执行的 DAC 访问限制 |
| CAP_DAC_WRITE  | 忽略文件写的 DAC 访问限制 |
| CAP_DAC_READ_SEARCH  | 忽略文件读及目录搜索的 DAC 访问限制 |
| CAP_FOWNER  | 忽略文件属主 ID 必须和进程用户 ID 相匹配的限制 |
| CAP_KILL  | 允许对不属于自己的进程发送信号 |
| CAP_SETGID  | 允许改变进程的 GID |
| CAP_SETUID  | 允许改变进程的 UID |
| CAP_NET_BIND_SERVICE  | 允许绑定到小于 1024 的端口 |
| CAP_NET_BROADCAST  | 允许网络广播和多播访问 |
| CAP_NET_ADMIN  | 允许执行网络管理任务 |
| CAP_NET_RAW  | 允许使用原始套接字 |
| CAP_FS_MOUNT  | 允许使用 chroot() 系统调用 |
| CAP_FS_FORMAT  | 允许使用文件格式 |
| CAP_SCHED_SETPRIORITY  | 允许设置优先级 |
| CAP_SET_TIMEOFDAY  | 允许设置系统时间 |
| CAP_CLOCK_SETTIME  | 允许改变系统时钟 |
| CAP_CAPSET  | 允许设置任意的 capabilities |
| CAP_REBOOT  | 允许重新启动系统 |
| CAP_SHELL_EXEC  | 允许执行shell |

## 规格说明

 - UserContainer容器最多3个层级：level0、level1、level2。

 - UserContainer容器最大数为系统进程最大数（采用宏配置）。

## 编译开关

 - 本模块功能特性采用编译宏 "LOSCFG_USER_CONTAINER" 进行开关控制，y打开，n关闭，默认关闭。

        config USER_CONTAINER
            bool "Enable USER container"
            default n
 
 - 本模块测试用例采用编译宏 "LOSCFG_USER_TEST_USER_CONTAINER" 进行开关控制，enable打开，disable关闭，默认关闭。