# 1. 概述

互斥锁又称互斥型信号量，用于实现对共享资源的独占式处理。当有任务持有时，这个任务获得该互斥锁的所有权。当该任务释放它时，任务失去该互斥锁的所有权。当一个任务持有互斥锁时，其他任务将不能再持有该互斥锁。多任务环境下往往存在多个任务竞争同一共享资源的应用场景，互斥锁可被用于对共享资源的保护从而实现独占式访问。

通过 mutex 操作相关 POSIX 接口可以设置、获取互斥锁的相关属性，包括使互斥锁保持一致，设置和获取互斥锁优先级上限、设置互斥锁属性的类型、协议、强健属性等。

# 2. 需求分析

## 2.1 原始需求描述

mutex 操作相关 POSIX 接口实现：

pthread_mutex_consistent

pthread_mutex_setprioceiling

pthread_mutex_getprioceiling

pthread_mutexattr_settype

pthread_mutexattr_setprotocol

pthread_mutexattr_setrobust

pthread_setconcurrency

# 3. 数据结构设计

## 3.1 原始数据结构设计

	typedef struct { 
		union { 
			int __i[sizeof(long)==8?10:6]; 
			volatile int __vi[sizeof(long)==8?10:6]; 
			volatile void *volatile __p[sizeof(long)==8?5:6]; 
		} __u; 
	} pthread_mutex_t;

	#define _m_type __u.__i[0]
	#define _m_lock __u.__vi[1]
	#define _m_waiters __u.__vi[2]
	#define _m_prev __u.__p[3]
	#define _m_next __u.__p[4]
	#define _m_count __u.__i[5]

	typedef struct {
		unsigned __attr; 
	} pthread_mutexattr_t;
	
	enum {
		LOS_MUX_PRIO_NONE = 0,
		LOS_MUX_PRIO_INHERIT = 1,
		LOS_MUX_PRIO_PROTECT = 2
	};

	enum {
		LOS_MUX_NORMAL = 0,
		LOS_MUX_RECURSIVE = 1,
		LOS_MUX_ERRORCHECK = 2,
		LOS_MUX_DEFAULT = LOS_MUX_RECURSIVE
	};

# 4. 接口设计

 - pthread_mutex_consistent
    - 函数原型：int pthread_mutex_consistent(pthread_mutex_t *m)
    - 函数功能：使互斥锁保持一致。
    - 返回值：EINVAL
    - 描述：m 指定的值无效。
    - 返回值：EPERM
    - 描述：调用方无权执行该操作。
    - 返回值：0
    - 描述：设置成功。

 - pthread_mutex_setprioceiling
    - 函数原型：int pthread_mutex_setprioceiling(pthread_mutex_t *restrict m, int prioceiling, int *restrict old)
    - 函数功能：设置互斥锁优先级上限属性。pthread_mutex_setprioceiling（）可锁定互斥锁（如果未锁定的话）。或者一直处于阻塞状态，直到 pthread_mutex_setprioceiling（）成功锁定该互斥锁，更改该互斥锁的优先级上限并将该互斥锁释放为止。锁定互斥锁的过程无需遵守优先级保护协议。
    - 如果 pthread_mutex_setprioceiling（）成功，则将在old中返回以前的优先级上限值。如果 pthread_mutex_setprioceiling（）失败，则互斥锁的优先级上限保持不变。
    - 返回值：EINVAL
    - 描述：m 或 prioceiling 指定的值无效。
    - 返回值：0
    - 描述：设置成功。

 - pthread_mutex_getprioceiling
    - 函数原型：int pthread_mutex_getprioceiling(const pthread_mutex_t *restrict m, int *restrict prioceiling)
    - 函数功能：获取互斥锁优先级上限属性。
    - 返回值：EINVAL
    - 描述：m 或 prioceiling 指定的值无效。
    - 返回值：0
    - 描述：获取成功。

 - pthread_mutexattr_settype
    - 函数原型：int pthread_mutexattr_settype(pthread_mutexattr_t *a, int type)
    - 函数功能：设置指定互斥锁属性的类型属性。
    - 类型属性的缺省值为 PTHREAD_MUTEX_NORMAL。
    - type 参数指定互斥锁的类型。以下列出了有效的互斥锁类型：
    - PTHREAD_MUTEX_NORMAL
    - 描述：普通互斥锁，不会检测死锁。如果任务试图对一个互斥锁重复持有，将会引起这个线程的死锁。如果试图释放一个由别的任务持有的互斥锁，或者如果一个任务试图重复释放互斥锁都会引发不可预料的结果。
    - PTHREAD_MUTEX_DEFAULT
    - 描述：如果尝试以递归方式锁定此类型的互斥锁，则会产生不确定的行为。对于不是由调用线程锁定的此类型互斥锁，如果尝试对它解除锁定，则会产生不确定的行为。对于尚未锁定的此类型互斥锁，如果尝试对它解除锁定，也会产生不确定的行为。允许在实现中将该互斥锁映射到其他互斥锁类型之一。
    - PTHREAD_MUTEX_RECURSIVE
    - 描述：递归互斥锁，默认设置为该属性。在互斥锁设置为本类型属性情况下，允许同一个任务对互斥锁进行多次持有锁，持有锁次数和释放锁次数相同，其他任务才能持有该互斥锁。如果试图持有已经被其他任务持有的互斥锁，或者如果试图释放已经被释放的互斥锁，会返回错误码。
    - PTHREAD_MUTEX_ERRORCHECK
    - 描述：错误检测互斥锁，会自动检测死锁。在互斥锁设置为本类型属性情况下，如果任务试图对一个互斥锁重复持有，或者试图释放一个由别的任务持有的互斥锁，或者如果一个任务试图释放已经被释放的互斥锁，都会返回错误码。
    - 返回值：EINVAL
    - 描述：a 或 type 指定的值无效。
    - 返回值：0
    - 描述：设置成功。

 - pthread_mutexattr_setprotocol
    - 函数原型：int pthread_mutexattr_setprotocol(pthread_mutexattr_t *a, int protocol)
    - 函数功能：设置指定互斥锁属性的协议。
    - protocol 参数指定互斥锁属性的协议。以下列出了有效的互斥锁属性的协议：
    - LOS_MUX_PRIO_NONE
    - 描述：不对申请互斥锁的任务的优先级进行继承或保护操作。
    - LOS_MUX_PRIO_INHERIT
    - 描述：优先级继承属性，默认设置为该属性，对申请互斥锁的任务的优先级进行继承。在互斥锁设置为本协议属性情况下，申请互斥锁时，如果高优先级任务阻塞于互斥锁，则把持有互斥锁任务的优先级备份到任务控制块的优先级位图中，然后把任务优先级设置为和高优先级任务相同的优先级；持有互斥锁的任务释放互斥锁时，从任务控制块的优先级位图恢复任务优先级。
    - LOS_MUX_PRIO_PROTECT
    - 描述：优先级保护属性，对申请互斥锁的任务的优先级进行保护。在互斥锁设置为本协议属性情况下，申请互斥锁时，如果任务优先级小于互斥锁优先级上限，则把任务优先级备份到任务控制块的优先级位图中，然后把任务优先级设置为互斥锁优先级上限属性值；释放互斥锁时，从任务控制块的优先级位图恢复任务优先级。暂不支持。
    - 返回值：EINVAL
    - 描述：a 或 protocol 指定的值无效。
    - 返回值：ENOTSUP
    - 描述：protocol 指定的值暂不支持。
    - 返回值：0
    - 描述：设置成功。

 - pthread_mutexattr_setrobust
    - 函数原型：int pthread_mutexattr_setrobust(pthread_mutexattr_t *a, int robust)
    - 函数功能：设置指定互斥锁属性的强健属性。
    - robust 参数指定互斥锁的强健属性。以下列出了有效的互斥锁的强健属性：
    - PTHREAD_MUTEX_ROBUST
    - 描述：如果互斥锁的属主失败，则以后对 pthread_mutex_lock（）的所有调用将以不确定的方式被阻塞。
    - PTHREAD_MUTEX_STALLED
    - 描述：互斥锁的属主失败时，将会解除锁定该互斥锁。互斥锁的下一个属主将获取该互斥锁，并返回错误 EOWNWERDEAD。
    - 注：应用程序必须检查 pthread_mutex_lock（）的返回代码，查找返回错误 EOWNWERDEAD的互斥锁。
    - 返回值：EINVAL
    - 描述：a 或 robust 指定的值无效。
    - 返回值：0
    - 描述：设置成功。

 - pthread_setconcurrency
    - 函数原型：int pthread_setconcurrency(int val)
    - 函数功能：设置线程并行级别。可以用于提示系统，表明希望的并发度。pthread_setconcurrency（）设定的并发度只是对系统的一个提示，系统并不保证请求的并发度一定会被采用。
    - 返回值：EINVAL
    - 描述：val 指定的值为负数。
    - 返回值：0
    - 描述：设置成功。

# 5. 规格说明

不涉及。

# 6. 编译开关

不涉及。

# 7. 侵入点设计

不涉及。
