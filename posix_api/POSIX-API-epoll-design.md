# 文件监听相关POSIX接口设计

## 工作原理

	1.epoll 全称 eventpoll，是 内核实现IO多路复用（IO multiplexing）的一个实现。
	
	2.epoll 在 内核中申请了一个简易的文件系统，把原先的一个 select 或者 poll 调用分为了三个部分：
	 
	 - 调用 epoll_create 建立一个 epoll 对象（在 epoll 文件系统中给这个描述符分配资源）
	 
	 - 调用 epoll_ctl 向 epoll 对象中添加连接的套接字
	 
	 - 调用 epoll_wait 收集发生事件的连接
	
	这样只需要在进程启动的时候建立一个 epoll 对象，并在需要的时候向它添加或者删除连接就可以了。
	
	因此，在实际收集的时候，epoll_wait 的效率会非常高，因为调用的时候只是传递了发生 IO 事件的连接。
	
![POSIX接口epoll原理](images/epollPrinciple.jpg)

![POSIX接口epoll流程](images/epollFlowChart.jpg)


## 接口说明
1.
	函数原型 ：int epoll_create(int size);

	功能说明 ：创建一个 epoll 对象，返回该对象的描述符，注意要使用 close 关闭该描述符。

	参数说明 ：要求 size 大于 0 即可。

2.
	函数原型 ：int epoll_ctl(int epfd, int op, int fd, struct epoll_event *event);

	功能说明 ：操作控制 epoll 对象，主要涉及 epoll 红黑树上节点的一些操作，比如添加节点，删除节点，修改节点事件。

	参数说明

		epfd：通过 epoll_create 创建的 epoll 对象。

		op：对红黑树的操作，添加节点、删除节点、修改节点监听的事件，分别对应 EPOLL_CTL_ADD，EPOLL_CTL_DEL，EPOLL_CTL_MOD。

			添加事件：相当于往红黑树添加一个节点，每个客户端连接服务器后会有一个通讯套接字，每个连接的通讯套接字都不重复，所以这个通讯套接字就是红黑树的 key。

			修改事件：把红黑树上监听的 socket 对应的监听事件做修改。

			删除事件：相当于取消监听 socket 的事件。

		fd：需要添加监听的 socket 描述符，可以是监听套接字，也可以是与客户端通讯的通讯套接字。

		event：事件信息。
                          
				EPOLLIN:  监听 fd 的读事件。举例：如果客户端发送消息过来，代表服务器收到了可读事件。
				
				EPOLLOUT: 监听 fd 的写事件。如果 fd 对应的发数据内核缓冲区不为满，只要监听了写事件，就会触发可写事件。

3.
	函数原型 ：int epoll_wait(int epid, struct epoll_event *events, int maxevents, int timeout);

	功能说明 ：阻塞一段时间并等待事件发生，返回事件集合，也就是获取内核的事件通知。
	
	遍历双向链表，把双向链表里的节点数据拷贝出来，拷贝完毕后就从双向链表移除。

	参数说明

		epid：epoll_create 返回的 epoll 对象描述符。

		events：存放就绪的事件集合，这个是传出参数。

		maxevents：代表可以存放的事件个数，也就是 events 数组的大小。

		timeout：阻塞等待的时间长短，以毫秒为单位，如果传入 -1 代表阻塞等待。

	返回值说明
	
		>0 , 代表有几个我们希望监听的事件发生了
		
		=0 , timeout 超时时间到了
		
		<0 , 出错，可以通过 errno 值获取出错原因


## 接口内部实现

1. 函数简介

 - epoll_create：文件描述符创建

	调用 epoll_create 创建一个 epoll 对象。
	
	实现代码中最核心的代码就是 struct eventpoll *ep = (struct eventpoll *)calloc(1,sizeof(struct eventpoll)) 
	
	创建一个 struct eventpoll 对象，然后对其成员进行初始化。size用来告诉内核这个监听的数目一共有多大。
	这个参数不同于select()中的第一个参数，给出最大监听的fd+1的值。
	
	需要注意的是，当创建好epoll文件描述符后会占用一个fd值，在使用完epoll后，必须调用close()关闭，否则可能导致fd被耗尽。

 - epoll_ctl： 注册

	1.epoll的事件注册函数，epoll_ctl向 epoll对象中添加、修改或者删除感兴趣的事件，返回0表示成功，否则返回–1，此时需要根据errno错误码判断错误类型。
	
	2.它与select()不同是在监听事件时告诉内核要监听什么类型的事件，而是在这里先注册要监听的事件类型。
	
	3.epoll_wait方法返回的事件必然是通过 epoll_ctl添加到 epoll中的。

- epoll_wait： 消息通知方式，触发后续执行

	等待事件的产生，类似于select()调用。
	
	- 参数events用来从内核得到事件的集合，maxevents告之内核这个events有多大，这个 maxevents的值不能大于创建epoll_create()时的size
	
	- 参数timeout是超时时间（毫秒，0会立即返回，-1将不确定，也有说法说是永久阻塞）。
	
	- 该函数返回需要处理的事件数目，如返回0表示已超时。如果返回–1，则表示出现错误，需要检查 errno错误码判断错误类型。

2. 使用流程/原理

	epoll函数调用过程：
	
		socket/bind/listen/epoll_create/epoll_ctl/epoll_wait/accept/read/write/close

	初始化---->epoll_create，返回一个epoll 文件描述符

	listen----> 服务器创建监听fd1

	epoll_ctl(epfd, EPOLL_CTL_ADD, fd1, EPOLLIN)，--->将监听fd1注册为监听事件。
	
	循环 
	{
		epoll_wait----> 监听注册在epoll上 监听fd1事件的发生（就绪态），

			根据 wait 触发的事件个数，逐个处理：
			
				如果触发的类型为 监听socket fd1的响应事件，则建立客户端连接。
			
					- accept---> 服务器收到客户端连接请求，创建客户端通信 fd2
					
					- epoll_ctl(epfd, EPOLL_CTL_ADD, fd2, EPOLLIN)，---> 将客户端通信 fd2 添加注册。

				如果触发的类型为 客户端通信 socket fd2的事件，则：
				    
					- 进行数据收发的操作，直到连接断开后，删除事件，相当于取消监听
	}
	
	最后需要释放 epoll 文件描述符。
	
## 说明： epoll和select对比

select：

	select本质上是通过设置或者检查存放fd标志位的数据结构来进行下一步处理。这样所带来的缺点是：
	
	1、 单个进程可监视的fd数量被限制，即能监听端口的大小有限。
	
	2、 内核处理对socket进行扫描时是线性扫描，即采用轮询的方法，效率较低。
	
	3、需要维护一个用来存放大量fd的数据结构，这样会使得用户空间和内核空间在传递该结构时复制开销大。


epoll的优点：

	1、没有最大并发连接的限制，能打开的FD的上限远大于1024（1G的内存上能处理约10~100万个客户端）；
	
	2、效率提升，内核处理不是轮询的方式，不会随着FD数目的增加效率下降。
	
	3、内存拷贝减少了内核态与用户态之间的复制开销。	

## 规格说明

	不涉及

## 编译开关

	不涉及
