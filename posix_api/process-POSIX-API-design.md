# 进程操作相关POSIX接口设计

# Kernel-5.10 调查

## vfork

### 工作原理

### 数据结构设计
	
### 流程设计

## posix_spawn

### 工作原理

posix_spawn 传入可执行文件的路径path等参数，运行传入的可执行文件，完成子进程的创建，并且返回子进程PID。
    
特性：父进程阻塞；子进程共享父进程的虚拟地址空间，包括共享父进程堆栈。

### 数据结构设计

	struct posix_spawn_args
	{
 		sigset_t oldmask;
	 	const char *file;
	 	int (*exec) (const char *, char *const *, char *const *);
	 	const posix_spawn_file_actions_t *fa;
	 	const posix_spawnattr_t *restrict attr;
	 	char *const *argv;
	 	ptrdiff_t argc;
	 	char *const *envp;
	 	int xflags;
	 	int err;
	};
	
### 流程设计

先 clone 子进程，传入CLONE_VM|CLONE_VFORK|SIGCHLD标志位，创建子进程，子进程共享父进程地址空间，并且父进程阻塞直到子进程退出。

## sem_open/sem_unlink

### 工作原理
	
sem_open 创建并初始化有名信号量，或者打开一个已存在的有名信号量。sem_unlink删除有名信号量。

### 数据结构设计
	
### 流程设计

# LiteOS-A 设计

## vfork

### 工作原理

	1.vfork会产生一个新的子进程。

	2.vfork创建的子进程与父进程共享数据段。

	3.vfork创建的子进程先于父进程运行。

### 数据结构设计
	
### 流程设计

### 接口说明

 1. 函数原型：pid_t vfork(void);
	功能说明：创建一个新的子进程。子进程共享父进程地址空间，子进程先于父进程运行。
	返回值说明：
			>0，返回新创建的子进程的pid
			-1，创建子进程失败

## posix_spawn

### 工作原理

posix_spawn 传入可执行文件的路径path等参数，运行传入的可执行文件，完成子进程的创建，并且返回子进程PID。

特性：父进程阻塞；子进程共享父进程的虚拟地址空间，包括共享父进程堆栈。

### 数据结构设计

	struct args {
		int p[2];
		sigset_t oldmask;
		const char *path;
		const posix_spawn_file_actions_t *fa;
		const posix_spawnattr_t *restrict attr;
		char *const *argv, *const *envp;
	};

### 流程设计

先 clone 子进程，传入CLONE_VM|CLONE_VFORK|SIGCHLD标志位，创建子进程，子进程共享父进程地址空间，并且父进程阻塞直到子进程退出。

![posixe_spawn](../images/posixe_spawn.jpg)

### 接口说明

 1. 函数原型：int posix_spawn(pid_t *restrict res, const char *restrict path,
        const posix_spawn_file_actions_t *fa,
        const posix_spawnattr_t *restrict attr,
        char *const argv[restrict], char *const envp[restrict])

    功能说明：创建一个新的子进程，并执行指定文件。

    参数说明：
		res：子进程 pid（传出值）
		path：可执行文件的路径
		fa：文件操作对象，该对象指定要在子进程执行的与文件相关的操作
		attr：属性对象，该对象指定创建的子进程的各种属性
		argv：子进程中执行的程序的参数列表
		envp：子进程中执行的程序的环境

    返回值说明：
		=0，表示执行成功

 2. 函数原型：int posix_spawnp(pid_t *restrict res, const char *restrict file,
        const posix_spawn_file_actions_t *fa,
        const posix_spawnattr_t *restrict attr,
        char *const argv[restrict], char *const envp[restrict])

    功能说明：创建一个新的子进程，并执行指定文件。

    参数说明：
		pid：子进程 pid（传出值）
		file：可执行文件的文件名
		file_actions：文件操作对象，该对象指定要在子进程执行的与文件相关的操作
		attrp：属性对象，该对象指定创建的子进程的各种属性
		argv：子进程中执行的程序的参数列表
		envp：子进程中执行的程序的环境
    返回值说明：
		=0，表示执行成功
		
 3. 函数原型：int posix_spawn_file_actions_addchdir_np(posix_spawn_file_actions_t *restrict fa, const char *restrict path)
    功能说明：向文件操作对象添加改变工作目录的操作，将新进程的工作目录更改为 path
    参数说明：
		fa：文件操作对象，该对象指定要在子进程执行的与文件相关的操作
		path：新进程的工作目录
    返回值说明：
		=0，表示执行成功

 4. 函数原型：int posix_spawn_file_actions_addclose(posix_spawn_file_actions_t *fa, int fd)
    功能说明：向文件操作对象添加关闭操作
    参数说明：
		fa：文件操作对象，该对象指定要在子进程执行的与文件相关的操作
		fd：文件描述符
    返回值说明
		=0，表示执行成功
 5. 函数原型：int posix_spawn_file_actions_adddup2(posix_spawn_file_actions_t *fa, int srcfd, int fd)
    功能说明：向文件操作对象添加复制文件描述符的操作，将文件描述符fildes复制为newfildes
    参数说明：
		fa：文件操作对象，该对象指定要在子进程执行的与文件相关的操作
		srcfd：文件描述符
		fd：新文件描述符
    返回值说明
		=0，表示执行成功
 6. 函数原型：int posix_spawn_file_actions_addfchdir_np(posix_spawn_file_actions_t *fa, int fd)
    功能说明：向文件操作对象添加改变工作目录的操作，将新进程的工作目录更改为文件描述符为fd的目录
    参数说明：
		fa：文件操作对象，该对象指定要在子进程执行的与文件相关的操作
		fd：文件描述符
    返回值说明
		=0，表示执行成功
 7. 函数原型：int posix_spawn_file_actions_addopen(posix_spawn_file_actions_t *restrict fa, int fd, const char *restrict path, int flags, mode_t mode)
    功能说明：向文件操作对象添加打开操作
    参数说明：
		fa：文件操作对象，该对象指定要在子进程执行的与文件相关的操作
		fd：文件描述符
		path：文件路径
		mode：文件打开方式
    返回值说明
		=0，表示执行成功
 8. 函数原型：int posix_spawn_file_actions_destroy(posix_spawn_file_actions_t *fa)
    功能说明：销毁文件操作对象
    参数说明：
		fa：文件操作对象，该对象指定要在子进程执行的与文件相关的操作
    返回值说明
		=0，表示执行成功
 9. 函数原型：int posix_spawn_file_actions_init(posix_spawn_file_actions_t *fa)
    功能说明：初始化文件操作对象
    参数说明：
		fa：文件操作对象，该对象指定要在子进程执行的与文件相关的操作
    返回值说明：
		=0，表示执行成功

 10. 函数原型：int posix_spawnattr_destroy(posix_spawnattr_t *attr)
    功能说明：销毁属性对象
    参数说明：
		attr：属性对象
    返回值说明：
		=0，表示执行成功

 11. 函数原型：int posix_spawnattr_getflags(const posix_spawnattr_t *restrict attr, short *restrict flags)
    功能说明：从属性对象中获取标志位的值
    参数说明：
		attr：属性对象
		flags：属性标志位
    返回值说明：
		=0，表示执行成功

 12. 函数原型：int posix_spawnattr_getpgroup(const posix_spawnattr_t *restrict attr, pid_t *restrict pgrp)
    功能说明：从属性对象中获取进程组的值
    参数说明：
		attr：属性对象
		pgrp：新进程映像要加入的进程组
    返回值说明：
		=0，表示执行成功

 13. 函数原型：int posix_spawnattr_getsigdefault(const posix_spawnattr_t *restrict attr, sigset_t *restrict def)
    功能说明：从属性对象中获取强制默认信号处理的信号集
    参数说明：
		attr：属性对象
		def：进程映像中强制默认信号处理的信号集
    返回值说明：
		=0，表示执行成功

 14. 函数原型：int posix_spawnattr_getsigmask(const posix_spawnattr_t *restrict attr, sigset_t *restrict mask)
    功能说明：从属性对象中获取有效的信号掩码
    参数说明：
		attr：属性对象
		mask：新进程映像中有效的信号掩码
    返回值说明：
		=0，表示执行成功

 15. 函数原型：int posix_spawnattr_init(posix_spawnattr_t *attr)
    功能说明：初始化属性对象
    参数说明：
		attr：属性对象
    返回值说明：
		=0，表示执行成功

 16. 函数原型：int posix_spawnattr_setflags(posix_spawnattr_t * attr, short flags);
    功能说明：向属性对象设置标志位的值
    参数说明：
		attr：属性对象
		flags：属性标志位
    返回值说明：
		=0，表示执行成功

 17. 函数原型：int posix_spawnattr_setpgroup(posix_spawnattr_t *attr, pid_t pgrp)
    功能说明：向属性对象设置进程组的值
    参数说明：
		attr：属性对象
		pgrp：新进程映像要加入的进程组
    返回值说明：
		=0，表示执行成功

 18. 函数原型：int posix_spawnattr_setsigdefault(posix_spawnattr_t *restrict attr,
     const sigset_t *restrict sigdefault);
    功能说明：向属性对象设置强制默认信号处理的信号集
    参数说明：
		attr：属性对象
		sigdefault：进程映像中强制默认信号处理的信号集
    返回值说明：
		=0，表示执行成功

 19. 函数原型：int posix_spawnattr_setsigdefault(posix_spawnattr_t *restrict attr, const sigset_t *restrict def)
    功能说明：向属性对象设置有效的信号掩码
    参数说明：
		attr：属性对象
		def：新进程映像中有效的信号掩码
    返回值说明：
		=0，表示执行成功

 20. 函数原型：int posix_spawnattr_getschedparam(const posix_spawnattr_t *restrict attr,
	struct sched_param *restrict schedparam)
    功能说明：从属性对象中获取分配给新进程映像的调度参数
    参数说明：
		attr：属性对象
		schedparam：分配给新进程映像的调度参数
    返回值说明：
		=0，表示执行成功

 21. 函数原型：int posix_spawnattr_setschedparam(posix_spawnattr_t *restrict attr,
	const struct sched_param *restrict schedparam)
    功能说明：向属性对象设置分配给新进程映像的调度参数
    参数说明：
		attr：属性对象
		schedparam：分配给新进程映像的调度参数
    返回值说明：
		=0，表示执行成功

 22. 函数原型：int posix_spawnattr_getschedpolicy(const posix_spawnattr_t *restrict attr, int *restrict policy)
    功能说明：从属性对象中获取分配给新进程映像的调度策略
    参数说明：
		attr：属性对象
		policy：分配给新进程映像的调度策略
    返回值说明：
		=0，表示执行成功

 23. 函数原型：
int posix_spawnattr_setschedpolicy(posix_spawnattr_t *attr, int policy)
    功能说明：向属性对象设置分配给新进程映像的调度策略
    参数说明：
		attr：属性对象
		policy：分配给新进程映像的调度策略
    返回值说明：
		=0，表示执行成功

## sem_open/sem_unlink

### 工作原理
	
sem_open 创建并初始化有名信号量，或者打开一个已存在的有名信号量。sem_unlink删除有名信号量。

### 数据结构设计
	
### 流程设计

## 规格说明：
 
 - 不涉及
 
## 编译开关

 - 不涉及