# 1. linux5.10调查
 - [linux5.10 readlink实现流程](linux实现调查.md)

# 2. liteos_a设计
### 数据结构设计
```
/* 以下需要修改 */

//proc根节点的Operetions，由于VfsProcfsLookup只实现对普通节点的查找，所以此处改为VfsProcfsRootLookup，
//rootLookup中调用VfsProcfsLookup和新追加的针对pid节点的VfsProcfsPidLookup函数。
static struct VnodeOps g_procfsVops = {
	.Lookup = VfsProcfsLookup,
	.Getattr = VfsProcfsStat,
	.Readdir = VfsProcfsReaddir,
	.Opendir = VfsProcfsOpendir,
	.Closedir = VfsProcfsClosedir,
	.Truncate = VfsProcfsTruncate
};
```

```
/* 以下为追加 */

//pid节点的Operations
static struct VnodeOps procfsPidBaseVops = {
	.Lookup = VfsProcfsPidBaseLookup,
};

//pid下子目录的结构，例如ns,fd,net等
struct PidEntry {
	const char *name;
	unsigned int len;
	mode_t mode;
	const struct VnodeOps *vop;
	const struct file_operations_vfs *fop;
};


//PidEntry结构体变量，pid下子目录列表
static const struct ProcPidEntry procPidBaseEntries[] = {
{
    "ns", sizeof("ns"), S_IRUSR|S_IXUGO, procfsNSDirVops, {}},
    "fd", sizeof("fd"), S_IRUSR|S_IXUGO, procfsFdDirVops, {}},
    ...
}

//ns节点的Operations
static struct VnodeOps procfsNSDirVops= {
	.Lookup = VfsProcfsNSDirLookup,
	.Readdir = VfsProcfsNSDirReaddir,
};

//ns下子目录列表
static const struct ProcPidEntry procPidNSEntries[] = {
{
    "mnt", sizeof("mnt"), S_IFLNK | S_IRWXUGO, procfsNSMntVops, {}},
    "pid", sizeof("pid"), S_IFLNK | S_IRWXUGO, procfsNSPidVops, {}},
    "user", sizeof("user"), S_IFLNK | S_IRWXUGO, procfsNSUserVops, {}},
    "uts", sizeof("uts"), S_IFLNK | S_IRWXUGO, procfsNSUtsVops, {}},
    "net", sizeof("net"), S_IFLNK | S_IRWXUGO, procfsNSNetVops, {}},
}

//ns下，mnt节点的Operations（其他节点定义参照mnt）
static struct VnodeOps procfsNSMntVops= {
	.Readlink = VfsProcfsNSMntReadlink,
	.Getattr = VfsProcfsNSMntStat,
};

//fd节点的Operations
static struct VnodeOps procfsFdDirVops= {
	.Lookup = VfsProcfsFdDirLookup
};

//fd下，各文件描述符的Operations
static struct VnodeOps procfsFdNumVops= {
	.Readlink = VfsProcfsFdNumReadlink,
};

```

### 流程设计

- 系统调用部分（已存在）

从用户层接口readlink，系统调用SysReadlink，最终调用内核层readlink函数。由于该部分已经实现，所以只需要去实现内核readlink函数。

![syscall](readlink(liteos_a)_1.png)

readlink中，第一步调用VnodeLookup函数，根据路径取得最后子目录的vnode信息，。第二步调用vnode的Readlink操作取得链接信息。
```
third_party/NuttX/fs/vfs/fs_readlink.c
int readlink(char *pathname, char *buf, size_t bufsize)
{
    struct Vnode *vnode = NULL;

    ret = VnodeLookup(pathname, &vnode, 0);

    ...
    if (vnode->vop && vnode->vop->Readlink) {
        vnode->vop->Readlink(vnode, buf, bufsize)
    }
}
```

VnodeLookup中分解路径，从根目录开始，如果子目录已经在pathcache中存在，直接取得vnode信息。如果在pathcache中不存在，调用节点操作Lookup函数，生成子目录的vnode信息。因此需要针对各个vnode实现其Lookup函数。

![VnodeLookup](readlink(liteos_a)_2.png)

- Lookup实现。

以/proc/[pid]/ns/mnt为例需要实现下面这些Lookup函数。

![VnodeLookup](readlink(liteos_a)_3.png)

- Readlink实现。

Lookup时已经设置最后节点的VnodeOps，以mnt节点为例是procfsNSMntVops，调用Readlink时，会调用到VfsProcfsNSMntReadlink。
```
static struct VnodeOps procfsNSMntVops= {
	.Readlink = VfsProcfsNSMntReadlink,
	.Getattr = VfsProcfsNSMntStat,
};
```

VfsProcfsNSMntReadlink中，根据pid信息取得ProcessCB信息，从ProcessCB中取得namespace信息(nsCommon)，根据nsCommon.inum取得namespace number，格式化到用户态buffer中。
其他节点的Readlink实现与此类似。
```
VfsProcfsNSReadlink(struct Vnode vnode, char *buf, size_t bufsize)
{
	...
	processCB = OS_PCB_FROM_PID(pid);
	struct nsCommon *ns = ns_ops->get(ProcessCB);
	snprintf(buf, size, "%s:[%u]", vnode->data->name, ns->inum)
	...
}
```

- fd节点的Lookup和Readlink

针对/proc/[pid]/fd/[文件描述符]，需要实现fd节点的Lookup操作，以及fd目录下各文件描述符的Readlink操作。

```
static struct VnodeOps procfsFdDirVops= {
	.Lookup = ProcfsFdLookup
};
```

ProcfsFdLookup函数中，为fd目录下文件分配vnode，设置vnode操作为procfsFdNumVops。
```
    (void)VnodeAlloc(&procfsFdNumVops, &node);

```

因为fd目录下文件均为软连接形式，procfsFdNumVops如下。
```
static struct VnodeOps procfsFdNumVops= {
	.Readlink = ProcfsFdSubReadlink,
};
```

ProcfsFdSubReadlink函数中，取得打开文件列表，根据fd取得文件实际路径，即为Readlink结果。
```
ProcfsFdSubReadlink(struct Vnode vnode, char *buf, size_t bufsize)
{
	...
	struct filelist *fileList = &tg_filelist;
	filp = &fileList->fl_files[fd];
	name = filp->f_path;
	strncpy_s(buffer, bufLen, name, count);
	...
}
```

# 3. 规格说明

不涉及

# 4. 编译开关

不涉及
