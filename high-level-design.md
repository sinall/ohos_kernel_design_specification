# 总体架构

## 概要设计

### 原理简图

**原理**

通过容器和资源配额的功能组合，为用户完成容器隔离。

![总体设计-原理图](images/总体设计-原理图.png)

- 备注：命名修改：

    - 容器隔离，采用名称：Container（容器）

    - 容器配额，采用名称：plimits，含义为：process limits（资源配额）

### 总体设计

**总图**

  - 每个进程PCB对应一个ContainerBundle，建立一一对应关系；且一个或多个PCB对应一个process limits（资源配额）

  - ContainerBundle节点成员为若干个指针，分别指向各容器链表的节点；process limits成员为若干个指针，分别指向各资源配额链表的节点。
![总体设计-总体结构-1](images/总体设计-总体结构-1.png)

**对比图**

  - 分析2个进程对比情况。

![总体设计-总体结构-2](images/总体设计-总体结构-2.png)

## 系统主要流程
### 初始化流程：

**系统初始化时，初始化容器5个头节点。并放入第一个进程 INIT 的 PCB 中**

目的是为后续准备：每次clone进程需要新建一个类型Container时，增长该类型Container链表（然后更新该PCB的ContainerBundle成员）。这5个链表分别是：UTC容器池、PID容器树、USER容器树、NET容器池、MNT容器池；

**系统初始化时，初始化资源配额3棵树头结点。并放入 第一个进程INIT的PCB中**

目的是为后续准备：每次需要新建一个资源配额策略时，增长该类型资源配额树（然后更新该PCB上的ProcessLimiter成员）。这3棵树分别是：调度参数权限树、内存限额树、PIDs调度树。（以及cgroupBundle树）

### 创建流程（clone）

**容器空间的创建**

针对主体：每个进程。首先检查是否需要创建新的容器；

1. 如果容器复用父进程，则在Bundle中复制父进程容器（目标ref+1）；

2. 如果容器指定要新建容器，则在Bundle中复制父进程容器（目标ref+1），并对某类型的全局容器链表进行增长，然后Bundle容器指针指向新节点。（原目标ref -1；新目标 ref+1）

  例如，根据flag，如果指定新 UTS 容器，则新建一个 UTS 容器节点，插入UTS容器链表末尾，然后Bundle指针指向新节点。（该节点ref +1）

   - （注意：如果容器链表超过3层，不允许新建，只能复用父进程容器）
  
  其他，都是类似，比如：UTC容器池、PID容器树、USER容器树、NET容器池、MNT容器池；

**资源限额的检查和限定处理**

  1. Memory资源限额。**内存配额检查时机**

    - 每次进程malloc时触发，计算进程当前内存总共占用大小，如果发现malloc内存后，导致会超出限额，则不申请内存，并返回OOM；
    
      如果计算结果不会超出限额，则继续申请内存。计算内存包括：（进程的实际占用内存空间，包括栈大小）
    
    - 调用stat查看统计值时触发检查，计算进程当前内存总共占用大小等参数。
    
      展示相应信息。如果发现进程当前内存总共占用大小已经大于阈值，则调用对应事件指令。

  2. 调度参数权限。

     配置进程设置调度参数的权限，包括允许设置进程权重等参数；详见详细设计）  
	         
  3. pids子进程数量控制。

     配置进程可创建的子进程数量；详见详细设计）  
			 
### 更新/切换流程（setns、 unshare）

**容器空间更新/切换**

针对主体：每个进程。用于更新容器空间（若干类容器）

根据flag，如果要更新 UTS 容器节点，则将Bundle对应容器指针指向新目标节点；（原容器节点ref -1；新目标ref+1）其他，都是类似，比如：UTC容器池、PID容器树、USER容器树、NET容器池、MNT容器池；
        
**进程资源限额的更新/切换**

资源限额plimits不涉及更新/切换

### 其他/限制

暂无。
