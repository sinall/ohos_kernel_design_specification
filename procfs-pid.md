# 1. liteos_a设计

## 数据结构设计

```
/* 以下为追加 */

//对于/proc根目录，因为增加了pid子目录的操作，需要对/proc节点的vops修改，用以下的g_procfsRootVops来替代原有的g_procfsVops.
static struct VnodeOps g_procfsRootVops = {
    .Lookup = VfsProcfsRootLookup,
    .Getattr = VfsProcfsStat,
    .Readdir = VfsProcfsRootReaddir,
    .Opendir = VfsProcfsOpendir,
    .Closedir = VfsProcfsClosedir,
    .Truncate = VfsProcfsTruncate
};
```


## 流程设计

执行"ls /proc/[pid]"时，会调用LsFile，最终调用/proc节点的Lookup取得pid节点的vnode。
执行"ls /proc"时，会调用LsDir，最终调用/proc节点的Readdir取得/proc下子节点，然后在stat中取得vnode，取得属性格式化显示。
流程图如下。

![ls](images/profs-pid.png)

- VfsProcfsRootReaddir实现

1.使用以下接口函数取得运行中进程pid。

```
pidMaxNum = LOS_GetSystemProcessMaximum();
pidList = (unsigned int *)malloc(pidMaxNum * sizeof(unsigned int));
pidNum = LOS_GetUsedPIDList(pidList, pidMaxNum);
```

2.针对pidList中每一个pid，检查是否已经在proc文件链表中，存在则什么也不做，不存在则创建对应的entry。

```
for (int i = 0; i < pidNum; i++) {
	snprintf(pidName, sizeof(pidName), "%u", pidList[i]);
	temp = entry->subdir;

	while (1)
	{
		// 到链表末尾还没有找到，则创建ProcDirEntry
		if (temp == NULL) {
			pn = ProcCreate(pidName, PID_DIR_MODE, entry, NULL);
			break;
		}
		// 如果已经存在则检查下一个pid
		if (ProcMatch(strlen(pidName), pidName, temp)) {
			break;
		}
		temp = temp->next;
	}
}
```

3.针对Proc文件链表中每一个pid节点的entry，检查它是否在pidList中，如果存在说明是运行中进程，什么也不做，
   如果在pidList中不存在，说明该进程已经结束，删除该pid节点的entry及vnode。

```
temp = entry->subdir;
while (temp != NULL)
{
	// name是字符串的时候，不做处理
	if (atoi(temp->name) <= 0) {
		temp = temp->next;
		continue;
	}
	for (i = 0; i < pidNum; i++) {
		// 存在的时候，检查下一个
		if (pidList[i] == atoi(temp->name)) {
			break;
		}
	}
	// 在pidList中不存在，则删除对应的entry
	if (i >= pidNum) {
		RemoveProcEntry(temp->name, entry);
		VnodeFree(currVnode)；

		entry->pdirCurrent = entry->subdir;
		entry->pf->fPos = 0;
	}
	temp = temp->next;
}
```

4.调用原有的VfsProcfsReaddir取得最新的dir信息。

```
return VfsProcfsReaddir(node, dir);
```

- VfsProcfsRootLookup实现

pid节点创建和删除的部分和VfsProcfsRootReaddir的处理一样，参照上面设计。
最后调用原有的VfsProcfsLookup从最新文件链表中查找。

```
return VfsProcfsLookup(parent, name, len, vpp);
```

# 2. 规格说明

不涉及

# 3. 编译开关

不涉及
