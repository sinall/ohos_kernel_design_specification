# 文件系统原理

## 通用文件模型
![avatar](../images/通用文件模型.png)
![avatar](../images/cgroup_进程与VFS对象之间的交互.png)
## 文件操作
![avatar](../images/cgroup_通过task检索文件.jpg)

# 数据结构
## kernfs文件系统
![avatar](../images/cgroup_kernfs和file-system的关系.png)

## cgroup挂在kernfs_node上
![avatar](../images/cgroup_kernfs中挂在cgroup和css.png)

## 文件读写过程操作过程
![avatar](../images/cgroup_subsys_文件操作.png)

# cgroup初始化
```
    start_kernel(){
	    + cgroup_init_early(); 
		+ page_cgroup_init();
		+ cgroup_init();
	}
```
- start_kernel->cgroup_init_early 初始化:
    + 初始化cgroup_root; 
	+ 初始化cgroup_subsys：cpu_cgrp_subsys;
	+ cgroup_init_subsys 中创建css(即struct cgroup_subsys_state);
- start_kernel->cgroup_init:
    + 初始化cftype: cgroup_init_cftypes;
	+ 初始化memory_cgrp_subsys: cgroup_init_subsys();
	+ css_alloc中创建cgroup控制信息结构：如 mem_cgroup; task_group等;
	+ cgroup_init_subsys->online_css 中把如 mem_cgroup; task_group等与tasks里的进程号对应的进程关联;
	+ cgroup_mkdir->create_css各级目录;