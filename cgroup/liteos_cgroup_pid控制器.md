# 1 需求  
pids控制器实现
- plimits.procs
- pids.max  

# 2 pids控制实现

pids，只管理进程，不管理线程
进程的操作包括：

- 创建：主要是clone(包括fork)过程，生成pid，需要添加到plimits里。  
- 退出：pid销毁，需要从plimits中移除。  
- 资源回收：涉及到内存统计。  
- 获取调度参数：涉及获取plimits.sched统计。  
- 获取进程ID：涉及获取相应的plimits。  

## 进程创建

### 初始进程创建

![avatar](../images/cgroup_liteos_processinit_cgroup根挂载.png)

- 初始化进程：1号进程，2号进程，将父plimits根挂在其pcb中;
- 其他进程创建，继承于1号进程，所以，同时继承父进程的plimits节点(将父plimits指针copy到子进程pcb上);

### clone(或fork)子进程操作

![avatar](../images/cgroup_liteos_pid_osclone过程分析.png)  

进程创建创建主要是clone(包括fork)过程，生成pid，需要添加到plimits里
- 从父pcb中获取ProcLimiterSet指针，添加到子pcb中;
- 设置pids.max,即该plimits管理的最大进程数，clone(或fork)process时，超过pids.max的限制，则clone(或fork)失败;通过如下方式配置pid个数限制：

```
writeproc [num] >> pids.max
```

### migrate操作

![avatar](../images/cgroup_liteos_pid_attach流程分析.png)

- 修改plimits.procs文件，添加pid的时候，先通过OS_PCB_FROM_PID(pid)找到precessCB，修改该processCB中的ProcLimiterSet指针的指向;可通过如下方式完成pid的迁移:
```
writeproc [pid] >> plimits.procs
```
- mem子系统同步修改：通过OS_PCB_FROM_PID(pid)找到precessCB，根据processCB，更新新、旧mem子系统的memory信息;
- 从原来的plimits中移除pid;

# 3 规格说明

1. 系统初始进程（0~14号进程）不允许进行迁移操作，挂载在根目录下。  
2. 迁移的pid必须是正在执行的进程。  
3. 挂载的pid个数不允许超过pids.max的限制。  

# 4 编译开关

- 本模块功能特性采用编译宏 “LOSCFG_PROCESS_LIMITS“进行开关控制，y打开，n关闭，默认关闭。

```
config PROCESS_LIMITS
    bool "Enable process limits"
    default n
```

- 本模块测试用例采用宏“LOSCFG_USER_TEST_PROCESS_LIMITS“进行开关控制，enable打开，disable关闭，默认关闭。

