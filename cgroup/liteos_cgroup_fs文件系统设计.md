# 1 概述

plimits的主要功能是限制进程组可以使用的资源数量，例如：memory，cpu，组内pid创建数量等。

## 1.1 基本概念

  -  **进程分组：**用户创建不同的目录，每一个目录，就是pid一个分组。每一个分组目录下，会有pid配置文件，将pid写入到pid配置文件，则该pid就属于这个组；   

  -  **限额配置：**用户通过修改pid分组目录下相关的限额配置文件的内容，达到控制进程组资源使用的目的，限额配置是以组为单位的。例如，修改memory.limit文件的值，会限制组内所有pid对应的进程总共的内存使用大小不会超过该限定值。  

  -  **限制器：**每个pid分组目录下，可能同时存在多种不同资源的控制文件，每一类控制文件文件，功能控制一类资源，例如：memory.*文件，控制内存使用限额，sched.*文件控制cpu使用限额。  

  -  **plimits操作：**plimits对配置文件的读写，最常用的方式是shell指令，写指令用writeproc，读指令用cat。  

## 1.2 术语说明

|       术语     |    英文全称            |      解释      |     备注                                  |
| :------------- | :--------------------- | :------------- | :---------------------------------------- |
| Plimits        | Process Limits         | 进程配额限制组 | 类似CGroup                                |
| Plimiter       | Process Limiter        | 进程配额限制器 | 类似CGroup Sub System Controller          |
| MemLimiter     | Process Memroy Limiter | 内存配额限制器 | 类似于CGroup Memory Sub System Controller |
| SchedLimiter   | Process Sched Limiter  | cpu配额限制器  | 类似于CGroup Cpu Sub System Controller    |
| PidsLimiter    | Process Pids Limiter   | 进程限制器     | 类似于CGroup Pids Sub System Controller   |



# 2. 需求分析

## 2.1 原始需求描述

支持plimits文件系统  

## 2.2 需求分析

plimits对文件系统的特殊需求

- plimits文件系统为伪文件系统：需要实现文件与plimits控制变量与文件的映射，通过文件操作，修改内核变量的目的。例如：memory限制器中，限制内存使用量的变量memlimit，通过用户修改memory.limit文件内容，即可修改变量memlimit的值，进而通过内核中memlimit的值限制内存分配；

- plimits文件系统需要能够被读写，目录能够被增删；

- plimits的目录，映射的是plimits的分组，所以，创建目录功能需要在创建目录的时候，自动创建目录下的文件（这些文件映射为限制器的控制变量），即创建限制器的文件。

- 创建限制器的文件是以组为单位创建的，例如：创建memory限制器，在增加一个memory限制器的时候，会全量创建所需的文件，而不是单独创建文件；

## 2.3 整体结构设计

  通过上文对文件系统需求的分析，复用procfs系统，可以实现plimitsfs的功能。  
  
  但是，procfs有不支持的功能，需要扩展实现：  

- 创建/删除目录：procfs目前有创建目录的相关接口实现，但是外层对其做了限制，实际没有用到，需要打通功能，实现目录创建的功能（仅限在plimits的根目录/proc/plimits及子目录以下）；

- procfs增删目录的扩展：plimits增删目录有特殊性，创建目录，需要同时创建出限制器文件，删除目录，需要同时删除限制器文件；

- procfs文件与内存变量的映射功能：通过修改plimitsfs的文件内容，实现修改内存变量的目的。这个功能，procfs架构是可以支持的；

**plimits目录规划：**  

|    权限    |   大小  |  用户  | 用户组 |         文件名         |
| ---------- | ------- | ------ | ------ | ---------------------- |
| -r--r--r-- | 0       | u:0    | g:0    | sched.stat             |
| -r--r--r-- | 0       | u:0    | g:0    | sched.quota            |
| -r--r--r-- | 0       | u:0    | g:0    | sched.period           |
| -r--r--r-- | 0       | u:0    | g:0    | memory.stat            |
| -r--r--r-- | 0       | u:0    | g:0    | memory.usage           |
| -r--r--r-- | 0       | u:0    | g:0    | memory.peak            |
| -r--r--r-- | 0       | u:0    | g:0    | memory.limit           |
| -r--r--r-- | 0       | u:0    | g:0    | memory.failcnt         |
| -r--r--r-- | 0       | u:0    | g:0    | pids.max               |
| -r--r--r-- | 0       | u:0    | g:0    | plimits.procs          |
| -r--r--r-- | 0       | u:0    | g:0    | plimits.limiter_delete |
| -r--r--r-- | 0       | u:0    | g:0    | plimits.limiter_add    |
| -r--r--r-- | 0       | u:0    | g:0    | plimits.limiters       |

- plimits文件系统复用proc文件系统

- plimits根目录：/proc/plimits/

- 只支持一个根目录（系统默认创建），不支持用户创建新的根目录

- plimits目录下，所有限制器文件在同一级目录下，以文件命名前缀标识不同限制器文件

## 2.4 相关操作

- 添加sched限制器

  操作后目录会新增sched.period、sched.quota、sched.stat文件

```
  writeproc sched >> plimits.limiter_add
```

- 删除sched配置器

  操作后目录会删除sched.period、sched.quota、sched.stat文件

```
 writeproc sched >> plimits.limiter_delete
```

- 添加memory限制器

  操作后目录会新增memory.failcnt、memory.limit、memory.peak、memory.usage、memory.stat文件

```
  writeproc memory >> plimits.limiter_add
```

- 删除memory限制器

  操作后目录会删除memory.failcnt、memory.limit、memory.peak、memory.usage、memory.stat文件文件

```
  writeproc memory >> plimits.limiter_delete
```


- 查看当前目录（即plimits）中包含多少个限制器

```
  cat plimits.limiters
```

- 创建子plimits节点

  在plimits根目录以下的目录中，创建子目录，即可以创建出来子plimits节点，子plimits节点会继承父节点的所有的plimiter和相关配置

```
  mkdir subdir_plimits
```

# 3. 数据结构设计

![数据结构](../images/liteos_plimits设计-数据结构.drawio.png)

1. ProcLimitsEntryOption

限制器`Entry`选项类，用于存储每个子限制器节点。

```
struct ProcLimitsEntryOption {
    UINT32 id;
    char name[PLIMITS_ENTRY_NAME_MAX];
    mode_t mode;
    uintptr_t offset;
    struct ProcFileOperations ops;
};
```

2. g_procfsVops 结构体变量用于`plimits`目录操作。

```
static struct VnodeOps g_procfsVops = {
    .Lookup = VfsProcfsLookup,
    .Getattr = VfsProcfsStat,
    .Readdir = VfsProcfsReaddir,
    .Opendir = VfsProcfsOpendir,
    .Closedir = VfsProcfsClosedir,
    .Truncate = VfsProcfsTruncate,
    .Mkdir = VfsProcfsMkdir,
    .Rmdir = VfsProcfsRmdir
};
```


# 4. 流程设计

## 4.1 文件系统初始化

1. 在Proc文件系统初始化过程中(既有的ProcFsInit函数)，创建plimits目录，将plimits下子文件信息存放在ProcDirEntry结构体的data成员中，作为子文件创建函数的入参。

```
void ProcFsInit(void)
{
    ProcLimitsInit(); //Proc文件系统初始化时调用
}

void ProcLimitsInit(void)
{
    ......
    struct ProcDirEntry *parentPDE = NULL;
    parentPDE = CreateProcEntry("plimits", S_IFDIR | S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH, NULL);
    parentPDE->data = (void *)&g_procLimiterSet;
    UINT32 mask = 0;
    mask |= (BIT(PROCESS_LIMITER_GENERAL) | BIT(PROCESS_LIMITER_ID_PIDS) |
                    BIT(PROCESS_LIMITER_ID_MEM) | BIT(PROCESS_LIMITER_ID_SCHED));
    ProcLimiterDirEntryInit(parentPDE, mask);      //创建子文件
    ......
}

//mask的值使用下面的枚举
enum ProcLimiterId {
    PROCESS_LIMITER_GENERAL = 0,
    PROCESS_LIMITER_ID_PIDS,
    PROCESS_LIMITER_ID_MEM,
    PROCESS_LIMITER_ID_SCHED,
    PROCESS_LIMITER_COUNT,
};
```

2. plimits目录下的默认文件信息
因为每次创建文件夹的同时默认文件也会被创建，将默认文件信息放在全局变量ProcLimiterSet中。
ProcLimiterSet的初始化时机，因为要统计内存进程等信息，所以放在内存及进程初始化之前比较好。
流程图如下。

![procLimiterSetInit](../images/cgroup_liteos_cgroupfs_init.png)

```
// 全局变量定义
ProcLimiterSet g_procLimiterSet;

//初始化函数
void OsProcLimitsInit(void)
{
    g_procLimiterSet.level = 0;
    g_procLimiterSet.parent = NULL;
    g_procLimiterSet.procLimiterList[g_procGeneralLimiterHeader.id] = &g_procGeneralLimiterHeader;
    g_procLimiterSet.procLimiterList[g_procPidLimiterHeader.id] = &g_procPidLimiterHeader;
    g_procLimiterSet.procLimiterList[g_procMemLimiterHeader.id] = &g_procMemLimiterHeader;
    g_procLimiterSet.procLimiterList[g_procSchedLimiterHeader.id] = &g_procSchedLimiterHeader;
}
```

g_procGeneralLimiterHeader定义如下（pid,mem,sched的ProcLimiter变量定义与此类似）：

```
static ProcLimiter g_procGeneralLimiterHeader = {
    .id = PROCESS_LIMITER_GENERAL,
    .private = (void *)&g_procGeneralLimiter,
    .parent = NULL,
};
```

3. 在/proc/plimits目录下创建默认的文件
首先，定义一个ProcLimitsEntryOption结构体类型的数组，存放所有的默认文件。数组中每个元素代表一个默认文件，包含了文件的名字,权限,操作的定义。
以pids.max为例，定义如下，其他文件的定义参照此实现。文件信息参照「其他→g_procLimitsEntryOpts全局变量」章节。

```
struct ProcLimitsEntryOption g_procLimitsEntryOpts[] = {
    {
        .id = PROCESS_LIMITER_ID_PIDS,
        .name = "pids.max",
        .mode = S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH,
        .ops = {
            .read = ProcLimitsShow64,
            .write = PidsMaxVariableWrite64,
        },
    },
    ......
}
```

其次，在ProcLimiterDirEntryInit函数中，循环取得g_procLimitsEntryOpts数组中成员，并使用name,mode,ops作为参数创建该文件对应的ProcDirEntry实例。

```
void ProcLimiterDirEntryInit(struct ProcDirEntry *dirEntry, UINT32 mask)
{
    ......
    ProcLimiterSet *procLimiterSet = (ProcLimiterSet *)dirEntry->parent->data;
    for (int i = 0; i < ARRAY_SIZE(g_procLimitsEntryOpts); ++i) 
    {
        opt = &g_procLimitsEntryOpts[i];
        plimiterId = opt->id;
        if (BIT(plimiterId) & mask) {
            pde = CreateProcEntry(opt->name, opt->mode, dirEntry);
            pde->procFileOps = &opt->ops;
            pde->data = procLimiterSet;
        }else{
            continue;
        }

    }
    ......
}
```

## 4.2 创建文件夹

可以在/proc/plimits目录或其子目录下创建文件夹，并且在创建文件夹的同时，文件夹下生成默认文件。
从以下流程图可以看出，为了支持mkdir，需要实现Vnode节点的Mkdir操作。

![mkdir](../images/cgroup_liteos_cgroupfs_mkdir.png)

1. VnodeOps中添加Mkdir操作

```
static struct VnodeOps g_procfsVops = {
    .Lookup = VfsProcfsLookup,
    .Getattr = VfsProcfsStat,
    .Readdir = VfsProcfsReaddir,
    .Opendir = VfsProcfsOpendir,
    .Closedir = VfsProcfsClosedir,
    .Truncate = VfsProcfsTruncate,
    .Mkdir = VfsProcfsMkdir    //追加
};
```

2. VfsProcfsMkdir函数实现
VfsProcfsMkdir函数中，调用既有的ProcCreate函数生成ProcDirEntry实例，调用既有的VnodeAlloc生成VnodeAlloc实例，
调用初始化时定义的ProcLimiterDirEntryInit函数生成默认文件。

```
int VfsProcfsMkdir(struct Vnode *parent, const char *dirName, mode_t mode, struct Vnode **vnode)
{
    ......
    struct Vnode *vp = NULL;
    struct ProcDirEntry *curEntry = NULL;
    VnodeAlloc(&g_procfsVops, &vp);
    curEntry = ProcCreate(dirName, S_IFDIR | mode, (struct ProcDirEntry *)(parent->data), NULL);
    
    ProcLimiterSet *parentPlimiterSet = (ProcLimiterSet *)(curEntry->parent->data);
    ProcLimiterDirEntryInit(curEntry, parentPlimiterSet->mask);    //在新创建的文件夹下生成默认文件
    ......
}
```

## 4.3 删除文件夹

从以下流程图可以看出，为了支持rmdir，需要实现Vnode节点的Rmdir操作。

![rmdir](../images/cgroup_liteos_cgroupfs_rmdir.png)

1. VnodeOps中添加Rmdir操作

```
static struct VnodeOps g_procfsVops = {
    .Lookup = VfsProcfsLookup,
    .Getattr = VfsProcfsStat,
    .Readdir = VfsProcfsReaddir,
    .Opendir = VfsProcfsOpendir,
    .Closedir = VfsProcfsClosedir,
    .Truncate = VfsProcfsTruncate,
    .Mkdir = VfsProcfsMkdir,    //mkdir时追加
    .Rmdir = VfsProcfsRmdir    //追加
};
```

2. VfsProcfsRmdir函数实现
VfsProcfsRmdir函数中，如果是/proc/plimits/下目录或子目录，允许删除，否则不允许删除。调用既有的RemoveProcEntry函数执行删除操作。

```
int VfsProcfsRmdir(struct Vnode *parent, struct Vnode *vnode, const char *dirName)
{
    struct ProcDirEntry *parentEntry = VnodeToEntry(parent);
    if (!strncmp(vnode->filePath, "/proc/plimits/", 14)) 
    {
        RemoveProcEntry(dirName, parentEntry);
    }
    ......
}
```

## 4.4 创建/删除文件组

创建文件夹的时候，会创建memory,pid及sched的所有默认文件。如果用户要删除memory限制器，需要删除memory关联的文件，保留pid,sched及共同文件，删除pid或sched时也同理。
创建/删除限制器时，将限制器名字写入plimits.limiter_add/plimits.limiter_delete文件，即执行创建/删除关联文件的操作。
所以需要在plimits.limiter_add和plimits.limiter_delete文件的Write操作中实现创建/删除文件组。

1. 创建文件组
解析plimits.limiter_add中写入的内容，和g_procLimitsEntryOpts数组中每个成员比较，一致时创建对应的ProcDirEntry实例。
例如写入的是"memory"关键字时，找出g_procLimitsEntryOpts中name以"memory"开头的成员，生成该name的文件。

```
//g_procLimitsEntryOpts中plimits.limiter_add的定义
{
    .id = PROCESS_LIMITER_GENERAL,
    .name = "plimits.limiter_add",
    .mode = S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH,
    .ops = {
        .write = ProcLimitsAddLimiters,
    },
},

ssize_t ProcLimitsAddLimiters(struct ProcFile *pf, const char *buf, size_t count, loff_t *ppos)
{
    UINT32 mask = 0;
    struct ProcDirEntry *dirEntry = pf->pPDE;
    ......

    if(strcmp(buf, "pids") == 0) {
        mask |= BIT(PROCESS_LIMITER_ID_PIDS);
    } else if (strcmp(buf, "memory") == 0) {
        mask |= BIT(PROCESS_LIMITER_ID_MEM);
    } else if (strcmp(buf, "sched") == 0) {
        mask |= BIT(PROCESS_LIMITER_ID_SCHED);
    }
    //调用以下共同函数，函数内部根据参数mask来决定生成哪一组的文件。
    ProcLimiterDirEntryInit(dirEntry, mask);
    ......
}
```

2. 删除文件组
解析plimits.limiter_delete中写入内容，和当前目录下ProcDirEntry（currDirection->subdir）的name比较，一致时删除该ProcDirEntry实例。

```
//g_procLimitsEntryOpts中plimits.limiter_delete的定义
{
    .id = PROCESS_LIMITER_GENERAL,
    .name = "plimits.limiter_delete",
    .mode = S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH,
    .offset = 0,
    .ops = {
        .write = ProcLimitsReleaseLimiters,
    },
}

ssize_t ProcLimitsReleaseLimiters(struct ProcFile *pf, const char *buf, size_t count, loff_t *ppos)
{
    pde = pf->pPDE;
    if(strcmp(buf, "pids") == 0) {
        ProcLimiterSetRemoveFiles(pde, "pids");
    } else if (strcmp(buf, "memory") == 0) {
        ProcLimiterSetRemoveFiles(pde, "memory");
    } else if (strcmp(buf, "sched") == 0) {
        ProcLimiterSetRemoveFiles(pde, "sched");
    }
    ......
}

void ProcLimiterSetRemoveFiles(struct ProcDirEntry *pde, char *prefix)
{
    struct ProcDirEntry *currDirection = pde->parent;    //取得父目录节点
    struct ProcDirEntry **iter = &currDirection->subdir;    //取得父目录下子节点链表
    while (*iter != NULL) {
        if (!strncmp((*iter)->name, prefix, strlen(prefix))) {    //名字一致时删除
            RemoveProcEntry((*iter)->name, currDirection);
            continue;
        }
        iter = &(*iter)->next;
    }    
}
```


## 4.5 其他

1. g_procLimitsEntryOpts全局变量

g_procLimitsEntryOpts中各成员信息如下表。

![method](../images/cgroup_liteos_cgroupfs_method.png)

- ProcLimitsAddLimiters

详细参照「创建文件组」章节。

- ProcLimitsReleaseLimiters

详细参照「删除文件组」章节。

- ShowPids

使用LOS_GetSystemProcessMaximum函数取得pid最大值，针对1~最大值之间的pid，调用OS_PCB_FROM_PID取得LosProcessCB数据。
因为在创建文件实例时，将ProcDirEntry->data指向ProcLimiterSet，proc文件系统中此处Read函数的第二参数data即为ProcDirEntry->data，
所以比较LosProcessCB->ProcLimiterSet和data相同时将LosProcessCB->processID写入第一参数，即为读取到的内容。
```
int ShowPids(struct SeqBuf *seqBuf, void *data)
{
    ......
    unsigned int pidMaxNum = LOS_GetSystemProcessMaximum();
    ProcLimiterSet *procLimiterSet = (ProcLimiterSet *)data;
    for (pid  = 1; pid < pidMaxNum; ++pid) {
        pcb = OS_PCB_FROM_PID(pid);
        if (pcb->procLimiterSet == procLimiterSet) {
            sprintf_s(seqBuf->buf, 4, "%d\n", pcb->processID);
        }
    }
    ......
}
```

- PidMigrateFromProcLimiterSet

因为在上记的Read函数中会循环检查每一个pid，所以在写的时候，对于写入的pid，只要将ProcLimiterSet信息写入到该pid的LosProcessCB->ProcLimiterSet即可。
```
ssize_t PidMigrateFromProcLimiterSet (struct ProcFile *pf, const char *buf, size_t count, loff_t *ppos)
{
    ......
    pid = (UINT32)atoi(buf);
    LosProcessCB *processCB = OS_PCB_FROM_PID(pid);
    ProcLimiterSet *procLimitsset = (ProcLimiterSet *)pf->pPDE->data;
    processCB->procLimiterSet = procLimitsset;
    ......
}
```

- ProcLimitsShow64

因为Write函数中将内容写入到ProcDirEntry->data中，而Read函数的第二参数data即为ProcDirEntry->data。所以直接将data赋值给seqBuf。
```
int ProcLimitsShow64(struct SeqBuf *seqBuf, void *data)
{
    UINT64 *value = (UINT64 *)data;
    LosBufPrintf(seqBuf, "%ld\n", (UINT64)*value);
}
```

- PidsMaxVariableWrite64

对写入的最大值进行检查，如果小于当前限制器中的进程个数，则提示错误并返回，否则记录传入的最大值。
```
ssize_t PidsMaxVariableWrite64(struct ProcFile *pf, const char *buf, size_t count, loff_t *ppos)
{
    ......
    UINT32 pidMax = atoi(buf);
    ProcLimiterSet *procLimitsSet = pf->pPDE->parent->data;
    for (int pid = 1; pid < LOS_GetSystemProcessMaximum(); ++pid) {
        pcb = OS_PCB_FROM_PID(pid);
        if (procLimitsSet == pcb->procLimiterSet) {
            pidCount++;
        }
    }
    if (pidMax <= pidCount) {
        PRINT_ERR("pids.max [%s] must be greater than [%d].\n", buf, pidCount);
        return -1;
    }
    *pf->pPDE->data = pidMax;
    return 0;
}
```

- ProcLimiterVariableWrite64

将写入的数据（第二参数buf），存放在当前节点的ProcDirEntry->data中。
```
ssize_t ProcLimiterVariableWrite64(struct ProcFile *pf, const char *buf, size_t count, loff_t *ppos)
{
    UINT64 *value = (UINT64 *)pf->pPDE->data;
    *value = atoi(buf);
}
```


# 5. 规格说明

1. Plimits根目录在/proc/plimits下。  
2. /proc/plimits目录作为根目录，其下的所有文件只读，不能写（所有文件的值，为系统的最大值，不支持写），权限如下：  
![method](../images/plimits_liteos_filemode_root.png)  
3. 除了根目录，其他/proc/plimits子目录下文件全部都可读，部分可写（plimits.limiter_add、plimits.limiter_delete、plimits.procs、pids.max、sched.period、sched.quota、memory.limit）：  
![method](../images/plimits_liteos_filemode_default.png)  
4. 在/proc目录下只允许创建和删除（mkdir和rmdir）/proc/plimits/及其子目录下操作，其他的/proc子目录不允许创建和删除目录和文件。  
5. /proc/plimits/及其子目录下不允许增加或删除单独的文件。  
6. mkdir可以在/proc/plimits及其子目录下创建子目录，并且，创建目录下，会自动创建plimiter对应的文件（继承自上级目录）。  
7. 用户可配置plimits子目录最大数为系统进程最大数（采用宏配置）。  

# 6. 编译开关

- 本模块功能特性采用编译宏 “LOSCFG_PROCESS_LIMITS“进行开关控制，y打开，n关闭，默认关闭。

```
config PROCESS_LIMITS
    bool "Enable process limits"
    default n
```

- 本模块测试用例采用宏“LOSCFG_USER_TEST_PROCESS_LIMITS“进行开关控制，enable打开，disable关闭，默认关闭。
