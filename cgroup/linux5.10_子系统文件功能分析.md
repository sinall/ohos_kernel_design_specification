# 子系统设计

## cgroup通用属性

### cgroup.clone_children  
    这个文件只对cpuset（subsystem）有影响，当该文件的内容为1时，新创建的cgroup将会继承父cgroup的配置，即从父cgroup里面拷贝配置文件来初始化新cgroup，可以参考这里
### cgroup.event_control
    用于eventfd的接口
### cgroup.procs
    当前cgroup中的所有进程ID，系统不保证ID是顺序排列的，且ID有可能重复，将pid写入cgroup.procs，则该pid所在的线程组及该pid的子进程等都会自动加入到cgroup中。将pid写入tasks，则只限制该pid。
### cgroup.sane_behavior

### notify_on_release
    该文件的内容为1时，当cgroup退出时（不再包含任何进程和子cgroup），将调用release_agent里面配置的命令。新cgroup被创建时将默认继承父cgroup的这项配置。
### release_agent
    里面包含了cgroup退出时将会执行的命令，系统调用该命令时会将相应cgroup的相对路径当作参数传进去。 注意：这个文件只会存在于root cgroup下面，其他cgroup里面不会有这个文件。
	notify_on_release 和 release_agent 会一起使用
### tasks
    当前cgroup中的所有线程ID，系统不保证ID是顺序排列的

## pids控制器
    只包含通用属性

## sched子系统

### cpu.cfs_period_us
    用来配置时间周期长度，单位是us，取值范围1000~1000000：1ms ~ 1s
### cpu.cfs_quota_us
    用来配置当前cgroup在设置的周期长度内所能使用的CPU时间数，单位us，最小值为1000：1ms； t > 1000, -1表示不作限制
	cpu.cfs_period_us cpu.cfs_quota_us这两个文件可以一起用来限制CPU的使用，且需要两个文件一起配置。
### cpu.rt_period_us
    用来配置时间周期长度，单位us，最小值为1 ，t > 1
### cpu.rt_runtime_us
    用来配置当前cgroup在设置的周期长度内所能使用的CPU时间数
    rt_runtime_us 与 rt_period_us 组合使用可以对cgroup中的实时调度任务进行cpu时间限制，只可用于实时调度任务。rt_period_us用来配置进行cpu限制的单位时间周期，设置每隔多久cgroup对cpu资源的存取进行重新分配，rt_runtime_us用来配置在设置的时间周期内任务对cpu资源的最长连续访问时间，二者单位都是微秒（us）。
### cpu.shares
    可以用来设置相对值，当有多个cpu group时，会根据cpu.shares计算可以得到多少cpu资源
### cpu.stat
    和统计相关，统计了进程组使用CPU的情况，包含3个指标。nr_periods、nr_throttled、throttled_time

## memory子系统

### memory.failcnt
    进程组内存使用超过memory.limit_in_bytes而导致的内存分配失败次数
### memory.force_empty
    当向memory.force_empty文件写入0时（echo 0 > memory.force_empty），将会立即触发系统尽可能的回收该cgroup占用的内存。该功能主要使用场景是移除cgroup前（cgroup中没有进程），先执行该命令，可以尽可能的回收该cgropu占用的内存，这样迁移内存的占用数据到父cgroup或者root cgroup时会快些。
### memory.limit_in_bytes
    设置进程组可以使用的内存的上限，单位是字节(byte)
### memory.max_usage_in_bytes
    历史上进程组使用的内存的峰值大小 
### memory.move_charge_at_immigrate
    set/show controls of moving charges，
### memory.oom_control
    set/show oom controls.
### memory.pressure_level
    memory pressure level设置
### memory.soft_limit_in_bytes
	和memory.limit_in_bytes 的差异是，这个限制并不会阻止进程使用超过限额的内存，只是在系统内存不足时，会优先回收超过限额的进程占用的内存，使之向限定值靠拢。
### memory.stat
    当前进程组的内存使用情况的统计信息
### memory.swappiness
    set/show swappiness parameter of vmscan (See sysctl's vm.swappiness) 设置/显示 vmscan 的 swappiness 参数（即内核参数vm.swappiness，值越大内核越积极使用swap，值越小内核越积极使用物理内存）
### memory.usage_in_bytes
    此刻进程组使用的内存的大小
### memory.use_hierarchy
    set/show hierarchical account enabled. 已弃用，不应配置
### memory.memsw.failcnt
    进程组内存使用超过memory.memsw.limit_in_bytes而导致的内存分配失败次数
### memory.memsw.limit_in_bytes  
    设置进程组可以使用的内存和可以使用的交换分区的之和的上限，单位是字节(byte)
### memory.memsw.max_usage_in_bytes  
    历史上进程组使用内存和交换分区之和的峰值大小
### memory.memsw.usage_in_bytes
    此刻进程组使用的内存和交换分区之和的大小
### memory.kmem.failcnt
    进程组内核内存使用超过memory.kmem.limit_in_bytes而导致的内存分配失败次数
### memory.kmem.limit_in_bytes
    设置或查看内核内存的使用限制。
### memory.kmem.max_usage_in_bytes
    查看记录的最大内核内存使用量。
### memory.kmem.tcp.failcnt
    TCP缓冲区内存使用超过memory.kmem.tcp.limit_in_bytes而导致的内存分配失败次数
### memory.kmem.tcp.limit_in_bytes
    设置或查看TCP缓冲区的内存使用限制。
### memory.kmem.tcp.max_usage_in_bytes
    查看记录的最大TCP缓冲区内存使用量。
### memory.kmem.tcp.usage_in_bytes
    查看当前TCP缓冲区的内存使用量。
### memory.kmem.usage_in_bytes
    查看当前内核内存使用量。


## devices子系统

### devices.allow
    cgroup中的task

## devices子系统

### devices.allow  
    cgroup中的task能够访问的设备列表，格式为type major:minor access  
-     type表示类型,可以为 a(all), c(char), b(block）
-     major:minor代表设备编号，两个标号都可以用代替表示所有，比如:*代表所有的设备
-     accss表示访问方式，可以为r(read),w(write), m(mknod)的组合

### devices.deny
    cgroup中任务不能访问的设备，和上面的格式相同

### devices.list
    列出cgroup 中设备的黑名单和白名单
	

