# 1 概述  

memory限制器，通过配置memory.limit文件（内存使用限额），限制plimits组内的所有进程的内存使用总和不会超过限额。  

# 2 原理分析  

memory限制器，主要是两个功能：内存的统计和限制。  
统计功能：统计所有的task的内存使用量，进而统计进程组内所有进程的总共的内存使用总量（即内memory制器的memoryUsed变量）。  
限制：在内存分配的时候，判断memoryUsed是否超过限额，如果超过限额，则在分配内存前直接返回，分配内存失败。  

## 2.1 内存分配接口分析  

内存分配因为应用场景不一样，会有多种分配内存的接口，经过分析，内存分配接口有以下这些：  
![avatar](../images/plimits_liteos_memory_OsMemAlloc.png)  
+ OsMemAlloc/OsMemFree：从指定动态内存池中申请内存  

![avatar](../images/plimits_liteos_memory_OsLoadUserInit.png)  
+ OsLoadUserInit：load init 的相关配置的时候直接从硬件分配页  

![avatar](../images/plimits_liteos_memory_OsVmBootMemAlloc.png)  
+ OsVmBootMemAlloc：开机引导分配器分配内存,只有开机时采用的分配方式  

![avatar](../images/plimits_liteos_memory_OsGetL2Table.png)  
+ OsGetL2Table：获取L2页表,分配L2表(需物理内存)  

![avatar](../images/plimits_liteos_memory_LOS_VMalloc.png)  
+ LOS_VMalloc：对外接口，申请内核堆空间内存  

![avatar](../images/plimits_liteos_memory_ShmAllocSeg.png)  
+ ShmAllocSeg：为共享段分配物理内存  

![avatar](../images/plimits_liteos_memory_OsStackAlloc.png)  
+ OsStackAlloc：分配栈区  

![avatar](../images/plimits_liteos_memory_OsTaskStackAlloc.png)  
+ OsTaskStackAlloc：任务栈(内核态)内存分配,由内核态进程空间提供,即KProcess 的进程空间  


[memory调用关系图.pdf](memory调用关系图.pdf)

# 3 memory限制器设计  

## 3.1 数据结构  

![avatar](../images/cgroup_liteos_memory_数据结构.png)  
- remain：当前剩余可使用的配额内存  
- used：当前已使用的配额内存  
- limit：配额限制使用的最大内存  
- stat：内存使用的一些统计信息，例如：内存分配失败次数（failCnt）  
- pressureLevel：表示当前配额已经使用完了，但是还需要申请内存  

## 3.2 memory控制实现  

![avatar](../images/cgroup_liteos_mem流程分析.png)  

- 内存分配接口，获取当前processCB，进而获取到plimits，根据分配内存大小，更新并统计内存使用量，结合配额判断分配成功/失败(count，size等信息)  
- 内存释放接口，获取当前processCB，进而获取到plimits，根据释放内存大小，更新并统计内存使用量  

## 3.2 memory控制器迁移操作

当plimits中的pid有迁移操作的时候，memory统计信息也需要迁移。  
memory统计信息迁移方法：根据task的内存统计信息，重新计算新的进程组的内存统计信息，将迁移的pid对应的task内存信息，统计到新的组里。
  

# 4 规格说明

memory限制器包含5个控制文件  
```
-r--r--r-- 0        u:0     g:0     memory.stat  
-r--r--r-- 0        u:0     g:0     memory.usage  
-r--r--r-- 0        u:0     g:0     memory.peak  
-r--r--r-- 0        u:0     g:0     memory.limit  
-r--r--r-- 0        u:0     g:0     memory.failcnt  
```

- memory.limit：内存使用限额，单位：byte
- memory.usage： 内存已使用量，单位：byte
- memory.peak：内存历史使用峰值，单位：byte
- memory.failcnt：因为超过限额，导致内存分配失败的次数
- memory.stat：统计值，供测试验证使用，包含：每个线程内存使用信息，方便测试验证使用

# 6 编译开关  

 - 本模块功能特性采用编译宏 “LOSCFG_PROCESS_LIMITS“进行开关控制，y打开，n关闭，默认关闭。  
```
config PROCESS_LIMITS
    bool "Enable process limits"
    default n
```
 
 - 本模块测试用例采用宏“LOSCFG_USER_TEST_PROCESS_LIMITS“进行开关控制，enable打开，disable关闭，默认关闭。 




