# 1 概述
sched限制器，通过配置sched.period文件（时间周期）和sched.quota（时间周期内cpu使用时间）两个值，限制plimits组内的所有进程，在时间周期内，使用的cpu时间总和不会超过限额时间长度。  

# 2 调度处理过程分析  

### 2.1 LOS_Schedule()任务调度与任务切换处理

调度限制器的实现，主要是在LOS_Schedule里实现，通过限制时间片的方式，限制调度时间

```
// 更新task时间片，更新任务队列，进行任务调度
VOID LOS_Schedule(VOID)
{
    runTask->ops->timeSliceUpdate(rq, runTask, OsGetCurrSchedTimeCycle());  // task剩余时间片和状态更新

    runTask->ops->enqueue(rq, runTask);  // 将当前任务加入到任务队列

    OsSchedResched();   // 任务切换
}

// 调用SchedTaskSwitch实现任务切换
VOID OsSchedResched(VOID)
{
    LOS_ASSERT(LOS_SpinHeld(&g_taskSpin));
    SchedRunqueue *rq = OsSchedRunqueue();
#ifdef LOSCFG_KERNEL_SMP
    LOS_ASSERT(rq->taskLockCnt == 1);
#else
    LOS_ASSERT(rq->taskLockCnt == 0);
#endif

    rq->schedFlag &= ~INT_PEND_RESCH;
    LosTaskCB *runTask = OsCurrTaskGet();
    LosTaskCB *newTask = TopTaskGet(rq);
    if (runTask == newTask) {
        return;
    }
    SchedTaskSwitch(rq, runTask, newTask);
}
```

### 2.2 触发任务调度的时机分析 

#### 2.2.1 主动任务调度

![avatar](../images/plimits_liteos_sched_LOS_Schedule.png)    

以下时机会主动触发任务调度，切换任务：  
LOS_SpinTrylock、LOS_SpinUnlock、LOS_SpinUnlockRestore、OsEventWrite、LOS_SemPost、OsQueueOperate、LOS_RwlockUnLock、LOS_MuxUnlock、OsSigSuspend、LOS_TaskCreate、LOS_TaskResume、OsFutexWake、LOS_TaskDelete、LOS_TaskPriSet、OsFutexRequeue、OsSetProcessScheduler、LOS_TaskUnlock、LOS_Schedule、LiteIpcWrite、LOS_TaskCpuAffiSet、LOS_SetTaskScheduler、LOS_TaskJoin、OsCopyProcess  

#### 2.2.2 定时任务调度

#### 定时器中，1ms调度一次

![avatar](../images/plimits_liteos_sched_SchedTimeoutTaskWake.png)

```
// 注册定时中断处理函数OsTickHandler
VOID HalClockInit(VOID)
{
    UINT32 ret;
    g_sysClock = HalClockFreqRead();//读取CPU的时钟频率
    ret = LOS_HwiCreate(OS_TICK_INT_NUM, MIN_INTERRUPT_PRIORITY, 0, OsTickHandler, 0);//创建硬中断定时器
    if (ret != LOS_OK) {
        PRINT_ERR("%s, %d create tick irq failed, ret:0x%x\n", __FUNCTION__, __LINE__, ret);
    }
}

// 时钟中断处理
VOID OsTickHandler(VOID)
{
    OsSchedTick();//由时钟发起的调度
}

// 由时钟发起的调度
VOID OsSchedTick(VOID)
{
    SchedRunqueue *rq = OsSchedRunqueue();
    if (rq->responseID == OS_INVALID_VALUE) {
        if (SchedTimeoutQueueScan(rq)) {   // 其中调用SchedTimeoutTaskWake()，唤醒超时任务
            LOS_MpSchedule(OS_MP_CPU_ALL);
            rq->schedFlag |= INT_PEND_RESCH;
        }
    }
    rq->schedFlag |= INT_PEND_TICK;
    rq->responseTime = OS_SCHED_MAX_RESPONSE_TIME;
}

// 超时任务调度
VOID SchedTimeoutTaskWake(SchedRunqueue *rq, UINT64 currTime, LosTaskCB *taskCB, BOOL *needSched)
{
    LOS_SpinLock(&g_taskSpin);
    UINT16 tempStatus = taskCB->taskStatus;
    if (tempStatus & (OS_TASK_STATUS_PENDING | OS_TASK_STATUS_DELAY)) {
        taskCB->taskStatus &= ~(OS_TASK_STATUS_PENDING | OS_TASK_STATUS_PEND_TIME | OS_TASK_STATUS_DELAY);
        if (tempStatus & OS_TASK_STATUS_PENDING) {
            taskCB->taskStatus |= OS_TASK_STATUS_TIMEOUT;
            LOS_ListDelete(&taskCB->pendList);
            taskCB->taskMux = NULL;
            OsTaskWakeClearPendMask(taskCB);
        }

        if (!(tempStatus & OS_TASK_STATUS_SUSPENDED)) {
#ifdef LOSCFG_SCHED_DEBUG
            taskCB->schedStat.pendTime += currTime - taskCB->startTime;
            taskCB->schedStat.pendCount++;
#endif
            taskCB->ops->enqueue(rq, taskCB);
            *needSched = TRUE;
        }
    }
    LOS_SpinUnlock(&g_taskSpin);
}

// LOS_SpinUnlock中调用LOS_Schedule进行任务调度
VOID LOS_SpinUnlock(SPIN_LOCK_S *lock)
{
    UINT32 intSave;
    LOCKDEP_CHECK_OUT(lock);
    ArchSpinUnlock(&lock->rawLock);

    intSave = LOS_IntLock();
    BOOL needSched = OsSchedUnlockResch();
    LOS_IntRestore(intSave);
    if (needSched) {
        LOS_Schedule();
    }
}
```

# 3 sched控制设计

根据以上原理分析，LOS_Schedule()是调度处理的入口，如果要对sched时间进行控制，对task的时间片以及状态进行控制，即可限制task的执行时间   

```
// 时间片更新的具体实现
VOID HPFTimeSliceUpdate(SchedRunqueue *rq, LosTaskCB *taskCB, UINT64 currTime)
{
    SchedHPF *sched = (SchedHPF *)&taskCB->sp;
    LOS_ASSERT(currTime >= taskCB->startTime);

    INT32 incTime = (currTime - taskCB->startTime - taskCB->irqUsedTime);

    LOS_ASSERT(incTime >= 0);

    if (sched->policy == LOS_SCHED_RR) {
        taskCB->timeSlice -= incTime;
#ifdef LOSCFG_SCHED_DEBUG
        taskCB->schedStat.timeSliceRealTime += incTime;
#endif
    }
#ifdef LOSCFG_PROCESS_LIMITS
    // 时间片控制限制
    // 此处如果根据plimits的判断结果，发现进程组的执行时间超过限额，则可以将irqUsedTime置0
    // irqUsedTime置0以后，schedFlag同时会被置为INT_PEND_RESCH状态
    // INT_PEND_RESCH状态的任务，只有在timeout的时候，被定时器唤醒，重新进入执行队列
    if (SchedLimiterIsExceed(taskCB->taskID, OsGetCurrSchedTimeCycle(), taskCB->startTime, taskCB->irqUsedTime) == TRUE) {
        taskCB->timeSlice = 0;
    }
#endif
    taskCB->irqUsedTime = 0;
    taskCB->startTime = currTime;
    if (taskCB->timeSlice <= OS_TIME_SLICE_MIN) {
        rq->schedFlag |= INT_PEND_RESCH;    // task状态也会被修改为INT_PEND_RESCH状态
    }

#ifdef LOSCFG_SCHED_DEBUG
    taskCB->schedStat.allRuntime += incTime;
#endif
}
```

### sched时间统计  

sched限制器中的两个关键值：  
- period： sched统计的时间周期  
- quota： sched统计周期内，可以使用的时间片总和  

统计方法：  
- 根据定时器，每隔period（单位：ns）更新一次周期起始时间，周期起始时间记为plimtStartTime  
- 统计plimits组内所有的task使用的时间片的总和  
- 如果plimits组内所有的task使用的时间片的总和allTime大于quota的限额，则通过上述的控制时间片的方法，限制task时间片（将剩余时间片taskCB->timeSlice置为0）  
- 周期起始时间plimtStartTime的更新：根据定时器的时间，如果时钟时间超过了plimtStartTime + period，则重置plimtStartTime为当前时间，并且将plimits组内所有的task使用的时间片的总和allTime置为0  


# 4 sched控制实现  

## 4.1 根据时间片进行控制  

### 时间周期更新

![avatar](../images/plimits_liteos_sched_OsPlimitsUpdateTime.png)  
（1）OsPlimitsUpdateTime：
- 更新plimits时间，如果超过周期时间，更新周期计数的起始时间

### 统计并限制剩余时间片

![avatar](../images/cgroup_liteos_sched_时间片更新流程.png)

# 5 规格说明

+ sched限制器包含三个控制文件
```
    -r--r--r-- 0        u:0     g:0     sched.stat
    -rw-r--r-- 0        u:0     g:0     sched.quota
    -rw-r--r-- 0        u:0     g:0     sched.period
```
+ sched.period：时间片统计周期，单位：ns
+ sched.quota： 时间片使用限额，是指在周期内，组内所有进程使用时间片总和，单位：ns
+ sched.stat：统计值，供测试验证使用，包含：每个线程上周期内使用的时间片信息，方便测试验证使用

# 6 编译开关  

 - 本模块功能特性采用编译宏 “LOSCFG_PROCESS_LIMITS“进行开关控制，y打开，n关闭，默认关闭。  
```
config PROCESS_LIMITS
    bool "Enable process limits"
    default n
```
 
 - 本模块测试用例采用宏“LOSCFG_USER_TEST_PROCESS_LIMITS“进行开关控制，enable打开，disable关闭，默认关闭。  
