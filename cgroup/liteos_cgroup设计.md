# 需求

- 支持plimits  
- 支持plimits文件系统  
- 支持pids控制器
- 支持sched控制器  
- 实现memory控制器  

# 术语说明
+ Plimits：Process Limits，进程配额限制组，类似CGroup;
+ Plimiter：Process Limiter，进程配额限制器，类似CGroup Sub System;
+ MemLimiter：Memroy Limister，内存配额限制器，类似与CGroup Memory Sub System;
+ SchedLimiter：Sched Limister，内存配额限制器，类似与CGroup Cpu Sub System;
+ PidsLimiter：Pids Limister，内存配额限制器，类似与CGroup Pids Sub System;

## 数据结构设计

### plimits数据结构
```
// ProcessLimits：进程配额组
typedef struct ProcessLimits {
    struct ProcessLimits *parent;
    ProcessLimiter *pLimiterList[PROCESS_LIMITER_COUNT];
    LOS_DL_LIST pidList;
	LOS_DL_LIST pidAll;
} ProcessLimits;
```
ProcessLimits包含了多个进程配额器ProcessLimiter
```
// ProcessLimiter：进程配额器
typedef struct ProcessLimiter {
    UINT8 id;
    struct ProcessLimiter *parent;
    LOS_DL_LIST childList;
    char private[0];
} ProcessLimiter;
```
进程配额器包含：pids配额器，memory配额器，sched配额器
```
typedef struct MemProcessLimiter {
    struct MemProcessLimiter *parent;
    LOS_DL_LIST childList;
    UINT64 remain;
    UINT64 used;
    UINT64 limit;
} MemProcessLimiter;
```
MemProcessLimiter挂在ProcessLimiter的private下
```
// 进程控制块
typedef struct ProcessCB {
    ......
    ProcessLimits *pLimits;
    ......
} LosProcessCB;
```
进程控制块包含了进程配额器指针，即当前进程属于进程配额器

```
ProcessLimits g_processLimit;
ProcessLimiter *g_processLimiters[PROCESS_LIMITER_COUNT];
```
