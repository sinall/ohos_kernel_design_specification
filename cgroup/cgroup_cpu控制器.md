# 数据结构
![avatar](../images/cgroup_subsys_cpu_task_cgroup数据结构.png)

struct sched_entity **se是一个指针数组，存了一组指向该task_group在每个cpu的调度实体。  
struct cfs_rq **cfs_rq也是一个指针数组，存在一组指向该task_group在每个cpu上所拥有的一个可调度的进程队列。  
parent、sibling和children三个指针负责将task_group连成一棵树。  

# 调度过程分析
![avatar](../images/cgroup_subsys_cpu_任务调度过程分析.png)
- 在定时器中断中我们会调用update_process_times来更新进程的时间统计信息。内部会调用scheduler_tick执行具体的处理
- 调用调度类实现的task_tick方法，针对CFS调度类该函数是task_tick_fair。CFS的周期性调度处理task_tick_fair
- 调用update_curr()更新当前运行的调度实体的虚拟时间等信息