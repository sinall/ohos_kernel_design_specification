# cgroup概述
## 控制子系统
控制子系统详细的数据结构见 [控制子系统数据结构详细说明](cgroup子系统说明.md)

####
![avatar](../images/cgroup_subsys_css关系说明.png)

### 子系统层级关系 
![avatar](../images/cgroup_subsys_目录层级_同一目录层级.png)
![avatar](../images/cgroup_subsys_目录层级_结构目录层级.png)


### pids子系统目录结构
![avatar](../images/cgroup_subsys_pids目录结构.png)
### cpu子系统目录结构
![avatar](../images/cgroup_subsys_cpu目录结构.png)
### memory子系统目录结构
![avatar](../images/cgroup_subsys_memory目录结构.png)

### devices子系统目录结构
![avatar](../images/cgroup_subsys_devices目录结构.png)
