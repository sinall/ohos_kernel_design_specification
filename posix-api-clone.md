# clone

clone接口为：

  int clone(int (*fn)(void *), void *stack, int flags, void *arg, ...
                 /* pid_t *parent_tid, void *tls, pid_t *child_tid */ );

  - 第一个入参fn用于传递子进程执行函数体；
 
  - 第二个入参stack用于指明子进程的用户空间堆栈。
 
  - 第三个入参flags用于指明clone要处理的标记flag。
 
		   其中命名空间相关flag处理如下：

		   通过clone创建命名空间时，采用符合POSIX标准的FLAG。名称和含义如下：

		  - CLONE_NEWUTS，为子进程创建 utsname 命名空间

		  - CLONE_NEWIPC，为子进程创建 IPC命名空间

		  - CLONE_NEWUSER，为子进程创建 user 命名空间

		  - CLONE_NEWPID，为子进程创建 pid 命名空间

		  - CLONE_NEWNET，为子进程创建 network 命名空间

		  - 如果clone时不指定上述任何FLAG，则子进程与父进程共享全部的命名空间。

  - 第四个入参arg用于指明传递给子进程的参数，为可变长参数。

通过clone接口可以完成创建子进程的操作，clone返回值：
 
 - 返回值=0：表示子进程执行；
 
 - 返回值>0：表示父进程执行，返回值为子进程的PID号；
 
 - 返回值<0：表示出错，进程创建失败；

# 通过clone接口创建子进程

通过clone接口创建子进程时，要检查FLAG。

 - 如果FLAG指明不需要为子进程新建任何命名空间，则子进程直接复用父进程的ContainerBundle，让子进程 ContainerBundle 指针直接指向父进程ContainerBundle指针指向的对象，并将该ContainerBundle对象引用计数递增（此处要用原子操作）。

伪代码操作方法是：

~~~
childProcessCB->containerBundle = runProcessCB->containerBundle;

atomic_inc(runProcessCB->containerBundle);
~~~

 - 如果FLAG指明要为子进程新建若干个命名空间，则步骤如下：

1. 新建一个containerBundle 节点（malloc申请内存），并将子进程ContainerBundle 指针指向该节点。

2. 然后，新建指定类型的命名空间节点，并进行初始化和复制。然后将ContainerBundle 节点中对应类型的容器指针指向这个新建的容器节点，并初始化该容器的引用计数为1（此处要用原子操作）。

3. 对于其他类型、不需要新建的容器，子进程仍然复用父进程的容器，办法是让子进程的ContainerBundle 节点中该类型容器的指针指向父进程该类型容器，并将父进程该类型容器的引用计数递增。

![命名空间创建过程](images/PID容器详细设计-创建过程-方案2.drawio.png)


### 特例：

#### 1. clone时对于**PID命名空间**的处理：

 - 当前进程创建子进程、孙子进程时，总是基于pidContainerForChildren（注：setns成功之后将目标容器记录在PID的pidContainerForChildren中）：

   - 如果未指定“CLONE_NEWPID”，则子进程放入到pidContainerForChildren指定的容器中。

   - 如果指定了“CLONE_NEWPID”， 则子进程放入到pidContainerForChildren指定的目标容器下一级容器中。（PID命名空间不可超过3层）

 - 获取和操作当前进程PID时，总是基于 pidContainer。

#### 2. 对于系统初始进程的处理：

系统启动时，为3个初始进程（0号:KIdle；1号:init；2号:KProcess），默认创建根容器，包括所有类型：UTS容器、NET容器、MOUNT容器、USER容器、PID容器。

# 规格说明

clone支持每一种容器最大数为系统进程最大数（采用宏配置）

# 编译开关

~~~
编译宏定义：
#ifdef LOSCFG_UTS_CONTAINER ------各容器编译宏
#ifdef LOSCFG_MOUNT_CONTAINER
#ifdef LOSCFG_PID_CONTAINER
#ifdef LOSCFG_NET_CONTAINER
#ifdef LOSCFG_USER_CONTAINER
#ifdef LOSCFG_CONTAINER // ----容器功能总编译宏
#ifdef LOSCFG_PROCESS_LIMITS // --PLIMIT编译宏
~~~
参考“命名空间详细设计”

