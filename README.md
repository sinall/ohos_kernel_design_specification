# 工业母机OS设计文档

#### 介绍
工业母机OS设计文档

[需求优先级](requirements.md)

#### 总体架构
[概要设计](high-level-design.md)


#### 容器隔离
  - 概要设计
     - [模块设计](container/container-low-level-design.md)
  - POSIX接口
    - [clone](posix-api-clone.md)
	- [unshare](posix-api-unshare.md)
	- [setns](posix-api-setns.md)
  - 文件系统命名空间
    - [文件挂载命名空间](mount-namespaces-design.md)
	- [chroot](chroot-design.md)
  - 进程命名空间
    - [进程命名空间](pid-namespaces-design.md)
    - [进程命名空间的增强](pid-namespaces-enhanced-design.md)
  - [用户命名空间](user-namespaces-design.md)
  - [主机命名空间](uts-namespaces-design.md)
  - [网络命名空间](net-namespaces-design.md)

#### 容器配额

  - [plimits概述](cgroup/liteos_cgroup设计.md)
  - [plimitsfs](cgroup/liteos_cgroup_fs文件系统设计.md)
  - [pids控制器](cgroup/liteos_cgroup_pid控制器.md)
  - [sched控制器](cgroup/liteos_cgroup_sched控制器.md)
  - [memory控制器](cgroup/liteos_cgroup_memory控制器.md)

#### procfs

  - [/proc下pid显示](procfs-pid.md)
  - [readlink](readlink/readlink设计.md)
  - [内核信息数据的保存和获取](proc-info.md)
  - [命名空间信息获取](namespace-info.md)
  - [用户态信息配置](procfs-sysctl-design.md)

#### POSIX接口

  - [进程操作相关POSIX接口实现](posix_api/process-POSIX-API-design.md)
  - [mutex操作相关POSIX接口实现](posix_api/process-mutex-API-design.md)
  - [文件监听相关POSIX接口实现](posix_api/POSIX-API-epoll-design.md)

#### inotify

  - [inotify](inotify-design.md)
