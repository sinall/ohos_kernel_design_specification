# chroot 设计

# 1. Kernel-5.10 调查

## 工作原理
`chroot()`系统调用设置当前`task_struct`下的`fs_struct`，从而达到隔离的目的。

## 数据结构设计
![chroot 数据结构](images/chroot-data-structure.png)

## 流程设计
![chroot 流程](images/chroot-flow.png)

# 2. LiteOS-A 设计

## 工作原理
`LosProcessCB.files`新增`rootdir`成员来替换全局变量`g_rootVnode`，将`chroot()`/`open()`等使用`g_rootVnode`的函数都改造为使用`LosProcessCB.files.rootdir`。

## 数据结构设计
![chroot 数据结构](images/liteos-chroot-data-structure.png)

## 流程设计
![chroot 流程](images/liteos-chroot-flow.png)

### chroot

根据传入参数path，调用既有的VnodeLookup取得该路径对应的Vnode实例。调用OsCurrProcessGet取得当前进程的LosProcessCB，将LosProcessCB->files->rootVnode设置为取得的Vnode实例。
```
int chroot(const char *path)
{
	......
	struct Vnode *vnode = NULL;
	VnodeLookup(path, &vnode, 0);
	LosProcessCB *curr = OsCurrProcessGet();
	curr->files->rootVnode = vnode;
	......
}
```

### VnodeLookup

查找路径的Vnode时，起始vnode为g_rootVnode。将起始vnode改为当前进程的LosProcessCB->files->rootVnode，因为chroot会改变此变量，即可读取到chroot后的rootVnode。
```
int VnodeLookupAt(const char *path, struct Vnode **result, uint32_t flags, struct Vnode *orgVnode)
{
	if (normalizedPath[1] == '\0' && normalizedPath[0] == '/') {
		......
		//*result = g_rootVnode;	//删除
		*result = GetCurrRootVnode();	//追加
		......
	}
}

static int PreProcess(const char *originPath, struct Vnode **startVnode, char **path)
{
	......
	//*startVnode = g_rootVnode;		//删除
	*startVnode = GetCurrRootVnode();	//追加
	......
}
```

### 初始化

LosProcessCB->files结构体初始化时，将rootVnode初始化为g_rootVnode。

```
struct files_struct *alloc_files(void)
{
	......
	files->rootVnode = g_rootVnode;	//追加
	......
}
```

# 3. 规格说明

不涉及

# 4. 编译开关

- 本模块功能特性采用编译宏“LOSCFG_CHROOT”进行开关控制，y打开，n关闭，默认关闭。

```
config CHROOT
    bool "Enable CHROOT"
    default n
    depends on FS_VFS
    help
      Answer Y to enable LiteOS support chroot.
```

- 本模块测试用例采用编译宏“LOSCFG_USER_TEST_CHROOT”进行开关控制，enable打开，disable关闭，默认关闭。
