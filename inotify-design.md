# inotify 设计

# Kernel-5.10 调查

## 工作原理

inotify提供了一种监控文件系统事件的机制，可以用来监控单个的文件以及⽬录。inotify机制借鉴了内核的notify通知链技术。

## 数据结构设计
![inotify 数据结构](images/inotify-data-structure.png)

## 流程设计
![inotify 流程](images/inotify-flow.png)

# LiteOS-A 设计

# 1. 概述

inotify API提供了一种监视文件系统的机制事件。Inotify可用于监视单个文件（文件添加、删除、读写），并通知用户。

# 2. 需求分析

## 2.1 原始需求描述

支持inotify机制。

## 2.2 inotify机制需求分析

- 提供inotify机制，用于监视文件系统变化，对inotify单个文件可以监视其读写、删除的操作，对目录，能够监视该目录下创建文件的操作（每种操作表示一种事件），并通知用户；
- 提供接口用于添加监视文件和删除监视文件；
- 可以设置监视对象的事件类型；
- 可以通过read接口读取触发的事件类型；

## 2.3 inotify整体结构设计

参考其它系统的设计，对于文件的监视用伪文件来实现，用户可以通过读取伪文件来返回监视文件的事件信息。当前liteos_a系统中的procfs文件系统为伪文件系统，可以复用procfs系统，实现对inotify伪文件的管理。

inotify伪文件目录结构如下：

![](images/liteos_inotify_pseudo_files_directory.png)

当前文件系统通过Vnode来管理文件，因此需扩展Vnode结构来记录所需的inotify信息。


分析具体应用场景后，确定inotify的实现使用observer设计模式，
在Vnode新增两个成员：

EventObserver： 用于记录被监视文件的事件列表

EventSubject： 用于记录监视该文件的inotify的Vnode指针列表

结合功能设计来看，inotify的具体工作流程如下：
1. 用户使用inotify功能时，先创建inotify实例，内核会返回其对应的fd。
2. 用户可以用该inotify监测对应文件的具体操作，通过addwatch 将该事件类型和inotify的Vnode指针记录到subject的VnodeInfo列表中。
3. 当操作被监视的文件时，文件会遍历监视者列表，如果有inotify监视该文件的对应事件，会将自己的文件路径和事件类型写入到对应inotify的事件列表中。
用户通过调用标准的read接口，可以读到对应inotify实例的事件列表，解析后就能查看到被操作的文件路径和事件类型。

工作原理如图所示：

![](images/liteos-inotify-theory.png)



# 3. 数据结构设计
新增4个数据结构，如下图:


![](images/liteos-inotify-data-structure.png)

# 4. 流程设计

## inotify初始化

1. 在Proc文件系统初始化时（ProcPmInit函数），创建inotify目录，将inotify实例对应的伪文件存放于该目录下。

```
void ProcPmInit(void)
{
    ......
    struct ProcDirEntry *inotify = CreateProcEntry("inotify", S_IFDIR | S_IRWXU | S_IRWXG | S_IROTH, NULL);
    ......
}
```

2. 初始化inotify实例。

- 在/proc/inotify下生成一个伪文件。

- 用Nuttx的open函数打开该文件，并返回其句柄

```
int DoInotifyInit()
{
    ......
    struct ProcDirEntry *curEntry = ProcCreate(name, S_IFREG, NULL, NULL);
    ......
    return open(filePath, O_RDWR);
}
```
## 添加监视对象

使用addWatch将inotify信息及事件添加到subject的inotify列表中。

1. 通过传入的inotify句柄，和被监视文件的路径分别获取到伪文件和真实文件的Vnode。

```
int DoInotifyAddWatch(int fd, const char * filePath, uint32_t eventType)
{
    struct Vnode *observerVnode = NULL;
    struct file *filep = NULL;
    struct Vnode* subjectVnode = NULL;

    fs_getfilep(fd, &filep);
    observerVnode = filep->f_vnode;
    ......
    VnodeLookup(filePath, &subjectVnode, 0);
    ......
    return InotifyUpdateWatch(observerVnode, subjectVnode, eventType);
}
```

2. 若真实文件的subject指向的observer列表中不存在当前监视，生成新的的节点记录VnodeInfo和事件类型，
并加入到observer列表中。

```
int InotifyUpdateWatch(struct Vnode *observerVnode, struct Vnode *subjectVnode, uint32_t eventType)
{
    ......
    if (newVnodeInfo != NULL) {
        newVnodeInfo->observerVnode = observerVnode;
        newVnodeInfo->eventType = eventType;
        newVnodeInfo->next = NULL;
    }
    ......
}

```

## 记录事件

用户在操作文件时，inotify记录监视文件路径和事件类型
1. 在write，read等事件触发时，调用写事件的方法。

```
int InotifyWriteEvent(const char *filePath, int eventType)
{
    ......
    return RecordInfoToObserver(filePath, eventType);
}

```
2. 遍历事件列表，并记录新事件

```
int RecordInfoToObserver(const char *filePath, uint32_t eventType)
{
    ......
    while (eventList != NULL) {
        observerVnode = eventList->observerVnode;
        if (eventType & eventList->eventType) {
            ret = DoInotifyWrite(observerVnode, subjectVnode, filePath, eventType);
        }
        eventList = eventList->next;
    }

    return ret;
}

int DoInotifyWrite(struct Vnode* observerHead,  struct Vnode *subjectVnode, const char *filePath,
    uint32_t eventType)
{
    ......
        while (curObserver != NULL) {
    ......
        event = (struct InotifyEvent*)LOS_MemAlloc(m_aucSysMem0, sizeof(struct InotifyEvent) + pathSize);
    ......
        event->mask = eventType;
        strncpy(event->name, filePath, pathSize);
        event->len = pathSize;

        newObserver->inotifyEvent = event;
    ......
        break;
    }
    ......
}

```

## 读取触发的事件类型

用户通过read接口读取触发的inotify event列表。
inotify_event结构如下：

```
struct inotify_event {
    int wd;
    uint32_t mask, cookie, len;
    char name[0];
};
```

主要用到mask（事件类型，可以是掩码）和name（触发事件的文件路径）

1. 用户态操作：

调用标准read接口读取伪文件，返回事件列表，并解析按照inotify_event结构对读取的数据做解析来获取事件列表。
```
    len = read(fd, eventBuf, sizeof(eventBuf));
    ......
    for(char *ptr = eventBuf; ptr < eventBuf + len; ptr += eventTTotalLen) {
        event = (const struct inotify_event *) ptr;
        eventTTotalLen = sizeof(struct inotify_event) + event->len;
        if ((event->mask & eventType) && (event->len) && (!strcmp(event->name ,filepath))) {
            num++;
        }
    }
```
2. 内核实现

当read函数对inotify的伪文件做操作时，内核会读取伪文件Vnode中的事件列表

```
int DoInotifyRead(struct Vnode* observerVnode, char *buf, uint32_t bufSize)
{
    ......
    char* curBuf = NULL;
    curBuf = buf;
    do {
             event = curObserver->inotifyEvent;
    ......
            eventSize = sizeof(struct InotifyEvent) + event->len;
            totalReadSize += eventSize;
    ......
            memcpy((void *)curBuf, (const void *)event, eventSize);
        }
        curObserver = curObserver->next;
    } while (curObserver != NULL);
    ......
}
```


总体的流程设计图如下：
![](images/liteos-inotify-flow.png)


# 5. 规格说明
- 每个文件最多支持被记录1000个事件，如超过数量限制后，再次触发事件会删除最早的记录并更新。

# 6. 编译开关
 -  不涉及