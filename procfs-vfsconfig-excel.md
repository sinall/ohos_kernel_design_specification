# 源码
```
#ifndef _VFS_CONFIG_H_
#define _VFS_CONFIG_H_

#include "los_config.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif /* __cplusplus */
#endif /* __cplusplus */

#define PATH_MAX 256
#define CONFIG_DISABLE_MQUEUE   // disable posix mqueue inode configure

/* file system config */

#define CONFIG_FS_WRITABLE      // enable file system can be written
#define CONFIG_FS_READABLE      // enable file system can be read
#define CONFIG_DEBUG_FS         // enable vfs debug function


/* fatfs cache config */
/* config block size for fat file system, only can be 0,32,64,128,256,512,1024 */
#define CONFIG_FS_FAT_SECTOR_PER_BLOCK  64

/* config block num for fat file system */
#define CONFIG_FS_FAT_READ_NUMS         7
#define CONFIG_FS_FAT_BLOCK_NUMS        28

#ifdef LOSCFG_FS_FAT_CACHE_SYNC_THREAD

/* config the priority of sync task */

#define CONFIG_FS_FAT_SYNC_THREAD_PRIO 10

/* config dirty ratio of bcache for fat file system */

#define CONFIG_FS_FAT_DIRTY_RATIO      60

/* config time interval of sync thread for fat file system, in milliseconds */

#define CONFIG_FS_FAT_SYNC_INTERVAL    5000
#endif

#define CONFIG_FS_FLASH_BLOCK_NUM 1

#define CONFIG_FS_MAX_LNK_CNT 40

/* nfs configure */

#define CONFIG_NFS_MACHINE_NAME "IPC"   // nfs device name is IPC
#define CONFIG_NFS_MACHINE_NAME_SIZE 3  // size of nfs machine name


/* file descriptors configure */

#define CONFIG_NFILE_STREAMS        1   // enable file stream
#define CONFIG_STDIO_BUFFER_SIZE    0
#define CONFIG_NUNGET_CHARS         0
#define MIN_START_FD 3 // 0,1,2 are used for stdin,stdout,stderr respectively

#define FD_SET_TOTAL_SIZE               (FD_SETSIZE + CONFIG_NEXPANED_DESCRIPTORS)
#define FD_SETSIZE                      (CONFIG_NFILE_DESCRIPTORS + CONFIG_NSOCKET_DESCRIPTORS)
#define CONFIG_NEXPANED_DESCRIPTORS     (CONFIG_NTIME_DESCRIPTORS + CONFIG_NQUEUE_DESCRIPTORS)
#define TIMER_FD_OFFSET                 FD_SETSIZE
#define MQUEUE_FD_OFFSET                (FD_SETSIZE + CONFIG_NTIME_DESCRIPTORS)
#define EPOLL_FD_OFFSET                 (FD_SETSIZE + CONFIG_NTIME_DESCRIPTORS + CONFIG_NQUEUE_DESCRIPTORS)

/* net configure */

#ifdef LOSCFG_NET_LWIP_SACK             // enable socket and net function
#include "lwip/lwipopts.h"
#define CONFIG_NSOCKET_DESCRIPTORS  LWIP_CONFIG_NUM_SOCKETS  // max numbers of socket descriptor

/* max numbers of other descriptors except socket descriptors */

#define CONFIG_NFILE_DESCRIPTORS    512
#define CONFIG_NET_SENDFILE         1   // enable sendfile function
#define CONFIG_NET_TCP              1   // enable sendfile and send function
#else
#define CONFIG_NSOCKET_DESCRIPTORS  0
#define CONFIG_NFILE_DESCRIPTORS    512
#define CONFIG_NET_SENDFILE         0   // disable sendfile function
#define CONFIG_NET_TCP              0   // disable sendfile and send function
#endif

#define NR_OPEN_DEFAULT CONFIG_NFILE_DESCRIPTORS

/* time configure */

#define CONFIG_NTIME_DESCRIPTORS     0

/* mqueue configure */

#define CONFIG_NQUEUE_DESCRIPTORS    256

/* directory configure */

#define VFS_USING_WORKDIR               // enable current working directory

#define CONFIG_EPOLL_DESCRIPTORS    32

/* permission configure */
#define DEFAULT_DIR_MODE        0777
#define DEFAULT_FILE_MODE       0666

#define MAX_DIRENT_NUM 14 // 14 means 4096 length buffer can store 14 dirent, see struct DIR

#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cplusplus */
#endif /* __cplusplus */
#endif
```

# 表格整理

| 宏     | 值     | 注释   |
| :---- | :---- | :--------- |
| PATH_MAX | 256|  |
| CONFIG_DISABLE_MQUEUE |  | disable posix mqueue inode configure |
| CONFIG_FS_WRITABLE |  |   enable file system can be written|
| CONFIG_FS_READABLE |  |  enable file system can be read|
| CONFIG_DEBUG_FS |  | enable vfs debug function |
| CONFIG_FS_FAT_SECTOR_PER_BLOCK | 64 | config block size for fat file system, only can be 0,32,64,128,256,512,1024 |
| CONFIG_FS_FAT_READ_NUMS | 7 | config block num for fat file system |
| CONFIG_FS_FAT_BLOCK_NUMS | 28  | config block num for fat file system |
| <font color=red>CONFIG_FS_FAT_SYNC_THREAD_PRIO</font> | 10 | config the priority of sync task |
| <font color=red>CONFIG_FS_FAT_DIRTY_RATIO</font> | 60 | config dirty ratio of bcache for fat file system |
| <font color=red>CONFIG_FS_FAT_SYNC_INTERVAL</font> | 5000 | config time interval of sync thread for fat file system, in milliseconds |
| CONFIG_FS_FLASH_BLOCK_NUM | 1 |  |
| CONFIG_FS_MAX_LNK_CNT | 40 |  |
| CONFIG_NFS_MACHINE_NAME | "IPC" |  nfs device name is IPC|
| CONFIG_NFS_MACHINE_NAME_SIZE | 3 | size of nfs machine name |
| CONFIG_NFILE_STREAMS |  1  | enable file stream |
| CONFIG_STDIO_BUFFER_SIZE | 0 |  |
| CONFIG_NUNGET_CHARS | 0 |  |
| MIN_START_FD | 3 |  0,1,2 are used for stdin,stdout,stderr respectively |
| FD_SET_TOTAL_SIZE | (FD_SETSIZE + CONFIG_NEXPANED_DESCRIPTORS) |  |
| FD_SETSIZE | (CONFIG_NFILE_DESCRIPTORS + CONFIG_NSOCKET_DESCRIPTORS) |  |
| CONFIG_NEXPANED_DESCRIPTORS | (CONFIG_NTIME_DESCRIPTORS + CONFIG_NQUEUE_DESCRIPTORS)  |  |
| TIMER_FD_OFFSET | FD_SETSIZE |  |
| MQUEUE_FD_OFFSET | (FD_SETSIZE + CONFIG_NTIME_DESCRIPTORS) |  |
| EPOLL_FD_OFFSET | (FD_SETSIZE + CONFIG_NTIME_DESCRIPTORS + CONFIG_NQUEUE_DESCRIPTORS) |  |
| CONFIG_NSOCKET_DESCRIPTORS  | LWIP_CONFIG_NUM_SOCKETS |   max numbers of socket descriptor|
| CONFIG_NFILE_DESCRIPTORS | 512 |  |
| CONFIG_NET_SENDFILE | 1 |  enable sendfile function|
| CONFIG_NET_TCP | 1 | enable sendfile and send function |
| NR_OPEN_DEFAULT| CONFIG_NFILE_DESCRIPTORS |  |
| CONFIG_NTIME_DESCRIPTORS | 0 | time configure |
| CONFIG_NQUEUE_DESCRIPTORS |256  | mqueue configure|
| VFS_USING_WORKDIR| | enable current working directory|
| CONFIG_EPOLL_DESCRIPTORS |  32| |
| DEFAULT_DIR_MODE| 0777| |
| DEFAULT_FILE_MODE|0666 | |
| MAX_DIRENT_NUM|14 |14 means 4096 length buffer can store 14 dirent, see struct DIR |