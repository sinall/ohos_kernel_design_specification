# 进程命名空间设计

# Kernel-5.10 调查

## 工作原理

**进程类型**

进程划分为如下四种类型：

1. 普通PID：
这是Linux对于每一个进程都使用的划分，每一个进程都分配给一个PID，每一个PID都对应一个task_struct，每一个task_struct对应着相应的命名空间，和PID类型。

2. TGID：
线程组ID，这个是线程的定义，这个定义在clone时候使用CLONE_THREAD函数调用的时候进行：在一个进程中，如果以CLONE_THREAD标志来调用clone建立的进程就是该进程的一个线程，它们处于一个线程组，该线程组的ID叫做TGID。处于相同的线程组中的所有进程都有相同的TGID；线程组组长的TGID与其PID相同；一个进程没有使用线程，则其TGID与PID也相同。

3. PGID：
另外，独立的进程可以组成进程组（使用setpgrp系统调用），进程组可以简化向所有组内进程发送信号的操作，例如用管道连接的进程处在同一进程组内。进程组ID叫做PGID，进程组内的所有进程都有相同的PGID，等于该组组长的PID。

4. SID：
几个进程组可以合并成一个会话组（使用setsid系统调用），可以用于终端程序设计。会话组中所有进程都有相同的SID。

**PID命名空间**

命名空间是为操作系统层面的虚拟化机制提供支撑，如PID命名空间有层次关系。

![pid 层次关系图](images/pid-hierarchy.png)

在上图有四个命名空间，一个父命名空间衍生了两个子命名空间，其中的一个子命名空间又衍生了一个子命名空间。以PID命名空间为例，由于各个命名空间彼此隔离，所以每个命名空间都可以有 PID 号为 1 的进程；但又由于命名空间的层次性，父命名空间是知道子命名空间的存在，因此子命名空间要映射到父命名空间中去，因此上图中 level 1 中两个子命名空间的六个进程分别映射到其父命名空间的PID 号5~10。

命名空间增大了 PID 管理的复杂性，对于某些进程可能有多个PID——在其自身命名空间的PID以及其父命名空间的PID，凡能看到该进程的命名空间都会为其分配一个PID。因此就有：

**全局ID**

在内核本身和初始命名空间中具有唯一的ID表示唯一的进程。内核中的init进程就是初始命名空间。系统为每一个进程分配了ID号来标示不同的进程，无论是在一级命名空间还是在二级命名空间中的进程，都在初始命名空间进程中都申请了一个ID号用于管理。这样在父命名空间中就可以看到子命名空间中的进程了。

**局部ID**

也可以说是子命名空间中看到的ID号，这个ID只能在子命名空间中有用，在父命名空间中没有作用。

## 数据结构设计

![进程命名空间数据结构](images/pid-namespaces-data-structure.png)

## 流程设计

```
sys_fork / sys_vfork / sys_clone / kernel_thread
   └→ _do_fork
          └→ copy_process
                └→ copy_namespaces
                       └→ create_new_namespaces (if need)
                               └→ copy_pid_ns
```
![进程命名空间流程](images/pid-namespaces-flow.png)

# LiteOS-A 设计

## 工作原理

通过PID命名空间完成进程PID的隔离。

在内核level 0 命名空间中可以管理所有PID；在内核level0以下命名空间中，只能访问本命名空间中的进程。

目的：完成命名空间之间进程不可访问的隔离效果，同时内核可以通过level0对所有进程统一管理。

PID命名空间设计最多3个层级：level0、level1、level2。

好处：结构体的vpid成员，开辟一个3个成员的数组就可以了。比链表速度和简单度都好一些。

## 数据结构设计
![进程命名空间数据结构设计](images/liteos_a_pid_struct.png)

### 数据结构
1.1 VprocessID结构体字段含义说明：

  - numbers[3]：用于管理各层的PID信息（3层）（包括PID所属命名空间、PID数值）。

1.2 Vpid结构体字段含义说明：
 
  - number：对应level的pid值（虚拟pid）；
  - index： 该number在PidContainer结构体中占用vpidArray数组元组的下标；
  - ownerContainer：该number所属的 PidContainer指针。

1.3 PidContainer结构体字段含义说明：

  - refCount：原子计数；
  - level：该pidcontainer对应的层级；
  - parent： 父PidContainer指针；
  - containerBase： 容器基础属性；
  - vpidArray[64]:  虚拟pid分配池，管理同一个PidContainer中的虚拟pid。

1.4 VpidItem结构体字段含义说明：

  - vpid： 负责同一个PidContainer中的虚拟pid值，初始化为1-64；
  - status：虚拟pid使能标识，VPID_STATUS_USED已使用，VPID_STATUS_UNUSED没有使用；
  - processCB ：该虚拟pid对应的PCB指针。

1.5 containerBundle结构体字段含义说明：

 - pidContainer: pid命名空间信息-进程本身信息；
 - pidContainerForChild : pid命名空间信息-子进程从pidContainerForChild继承和使用pid命名空间信息；
 - ...... :其他命名空间信息。

### 数据结构关系说明
在原有LosProcessCB结构体的基础上，新增加成员VprocessID结构体和containerBundle指针，VprocessID结构体储存一个进程对应的每层pidcontainer的虚拟pid信息，containerBundle指针储存不同的命名空间信息。

每当新创建一个PID命名空间的时候，会新生成一个pidContainer保存并管理新命名空间的信息，包括记录level层级，以及使用vpidArray管理命名空间虚拟pid的分配和回收，当容器中新增加一个进程时，会遍历vpidArray，获取一个status为VPID_STATUS_UNUSED的状态的虚拟pid,分配到pcb->vProcessID.numbers[level].number,并将vpidArray的数组下标记录到pcb->vProcessID.numbers[level].index。


      while (level > PID_CONTAINER_ROOT_LEVEL) {
            processCB->vProcessID.numbers[level].number = AllocVpid(pidContainer, &vpidIndex);
            processCB->vProcessID.numbers[level].index = vpidIndex;
            processCB->vProcessID.numbers[level].ownerContainer = pidContainer;
            pidContainer->vpidArray[vpidIndex].processCB = processCB;
            pidContainer = pidContainer->parent;
            level = level - 1;
        }

对虚拟pid管理的时候只管理1层和2层的信息，liteos_a分配了64个PCB，每个PCB的进程号是固定的，初始化和回收初始化的时候随processCB->processID赋值到vProcessID.numbers[0].number。

进程退出释放资源的时候调用了`OsProcessResourcesToFree`函数，在里面新增`ExitProcessContainer`函数释放各个containerBundle的，pidContainer释放的时候，挨个释放每层的虚拟pid资源。

	while (level > PID_CONTAINER_ROOT_LEVEL) {
        vpidIndex = processCB->vProcessID.numbers[level].index;
        pidContainer->vpidArray[vpidIndex].status = VPID_STATUS_UNUSED;
        pidContainer = pidContainer->parent;
        level = level - 1;
    }
## 流程设计
![进程命名空间流程设计](images/liteos_a_pidcontainer.png)

```
SysFork / SysVfork / SysClone 
   └→ OsClone
          └→ OsCopyProcess
                └→ OsCopyProcessResources
                       └→ CopyContainers 
                               └→ CreateNewContainers (if need)
										└→ HandlePidContainer
```
### 基础设计
设计思路：
 0层默认使用liteos_a本身的功能，1层2层使用命名空间的功能，1层2层依托于0层的功能实现，基本思路是将虚拟pid转化成0层globalpid再处理。

使用宏LOSCFG_PID_CONTAINER 来实现PID命名空间功能的开关，配置文件Kconfig增加。

    config PID_CONTAINER
   	 	bool "Enable PID container"
        default y
pidcontainer和pidContainerForChild初始化流程：  `OsMain` -> `InitContainerBundle` -> `InitPidContainer`  。

增加全局结构体变量g_rootContainerBundle实现根ContainerBundle初始化，全局结构体变量g_rootPidContainer实现0层PID命名空间初始化。
系统启动，在`OsMain`中增加函数`InitContainerBundle`实现全局 ContainerBundle初始化，函数`InitContainerBundle`里面调用各个命名空间初始化。

	ContainerBundle g_rootContainerBundle = {
	#ifdef LOSCFG_PID_CONTAINER
    .pidContainer    = &g_rootPidContainer,
	.pidContainerforChild =&g_rootPidContainer,
	#endif
	};



  系统启动时，初始化g_rootContainerBundle，挂在init进程上面，pidContainer和pidContainerforChild默认指向同一个指针。

  - 创建进程时，默认复用父进程pidContainerforChild的pid命名空间；如果指定新建命名空间，则新命名空间层级是父进程pidContainerforChild命名空间层级+1，依次赋值上一层级pidcontainer里面的信息，完成新命名空间的新建。
  - vpid的获取规则：

    复用父进程pidContainerforChild命名空间时，轮询父进程命名空间中vpidArray状态status为VPID_STATUS_UNUSED，将对应的虚拟pid赋值到pcb->vProcessID.numbers[level].number,并将status改为 VPID_STATUS_USED，依次往上层映射赋值，规则一致；

    新建命名空间时对应新命名空间的虚拟pid为1，依次映射赋值每层命名空间的虚拟pid，规则同复用父进程命名空间的规则。

        for (index = 0; index < LOSCFG_BASE_CORE_PROCESS_LIMIT; index++) {
              if (pidContainer->vpidArray[index].status == VPID_STATUS_UNUSED) {
              pidContainer->vpidArray[index].status = VPID_STATUS_USED;
              *vpidIndex = index;
              break;
             }
   	    }

    获取进程PID时，先获取当前运行进程的level，然后在Vpid结构体中，得到对应number数值, 即为当前的虚拟pid。
    获取当前运行进程的level：
	OsCurrProcessGet()->containerBundle->pidContainer->level;
	获取vpid:
	OsCurrProcessGet()->vProcessID.numbers[level].number。

  - 终止进程时，依次释放各层级的PidContainer容器的vpidArray结构体里面被释放进程占用的虚拟pid，判断命名空间的引用计数，当计数为0时，释放命名空间，根据当前运行的虚拟pid得到globalpid,使用globalpid完成终止动作。

### 进程命名空间的隔离

新增`GetGlobalPidByCurrPid`根据虚拟pid取到globalPid，实现思路：

先获取运行进程层级，0层虚拟pid即 globalPid ；

非0层，获取运行进程pcb，从而获取运行进程的containerBundle->pidContainer->vpidArray，轮询vpid值,判断是否和传入vpid相等，相等的情况下，判断使能标志status是否是VPID_STATUS_USED使能状态，是的话，从vpidArray[index].processCB获取processID，再判断该PCB的vProcessID.numbers[level].ownerContainer是否和运行进程的pidContainer相等，相等即为globalPid，否则终止循环，返回-1，即当前查找的vpid不在运行进程的命名空间中。

	for (index = 0; index < LOSCFG_BASE_CORE_PROCESS_LIMIT; index++) {
        if (runProcessCB->containerBundle->pidContainer->vpidArray[index].vpid == pid) {
            if (runProcessCB->containerBundle->pidContainer->vpidArray[index].status == VPID_STATUS_USED) {
                globalPid = runProcessCB->containerBundle->pidContainer->vpidArray[index].processCB->processID;
                LosProcessCB *processCB =  runProcessCB->containerBundle->pidContainer->vpidArray[index].processCB;
                if(runProcessCB->containerBundle->pidContainer == processCB->vProcessID.numbers[level].ownerContainer) {
                    return globalPid;
                }
				else {
					break;
				}
            }
            else {
                break;
            }
        }
    }



新增`IsPcbBelongRunPidContainer`函数实现传入PCB是否和当前运行进程属于同pid命名空间，实现思路：

先获取运行进程层级，找到对应的全局globalPid，再判断是否和传入的PCB的globalPid相等。
	
	if (level > PID_CONTAINER_ROOT_LEVEL){
        UINT32 globalPid = (INT32)GetGlobalPidByCurrPid(processCB->vProcessID.numbers[level].number);
        if (globalPid == FINDGLOBALPID_ERR) {
            return -LOS_NOK;
        }
        if (processCB->processID != globalPid) {
            return -LOS_NOK;
        }
    }
### 1.1 clone 
`clone`函数调用`OsCopyProcess`函数copy一个进程的时候，调用了`OsCopyProcessResources`拷贝进程资源，在拷贝资源的时候，新增加一个`CopyContainers`函数拷贝或创建一个containerbundle,containerbundle下挂各个命名空间的指针，根据clone传入的flag依次选择拷贝还是新建各个命名空间，返回对应pidcontainer的vpid。

新增`HandlePidContainer`函数实现拷贝或者创建pidContainer：

当不传入CLONE_NEWPID标志的时候进行拷贝，返回父进程pidContainerforChild并将其计数+1，并在其命名空间和上层各命名空间分配虚拟pid值；

当传入CLONE_NEWPID标志的时候创建新的pidContainer，判断层级是否大于2层，大于报错，否则并进行初始化赋值（层级，虚拟pid分配数组，引用计数，父pidContainer）。

	if (level >= PID_CONTAINER_LEVEL_LIMIT) {
		PRINT_ERR("CLONE_NEWPID jsut Supports 3 layers \n"); 
	    goto out;
	}
    newPidContainer = CreatePidContainer();
    if (newPidContainer == NULL) {
        goto out;
    }
	 for (index = 0; index < LOSCFG_BASE_CORE_PROCESS_LIMIT; index++) {
       newPidContainer->vpidArray[index].vpid = index+1;
       newPidContainer->vpidArray[index].status = VPID_STATUS_UNUSED;
    }

    newPidContainer->level = level;
    newPidContainer->parent = runPidContainer;
    atomic_set(&newPidContainer->refCount, 1);

### 1.2 getpid

先获取当前运行进程的level，然后在调用`OsCurrProcessGet()`获取pcb,在pcb->vProccessID,numbers[level].number 即为当前的虚拟pid。


### 1.3 getppid

先获取当前运行进程的level，然后在调用`OsCurrProcessGet()`获取pcb，获取到父进程的全局PID：pcb->parentProcessID ,然后调用`OS_PCB_FROM_PID(parentTopProcessID)`获取父进程pcb，
在pcb->vProccessID,numbers[level].number 即为当前的虚拟pid；


### 1.4 kill 

在SysKill中将传入的pid转换成全局的globalPid，之后的逻辑校验不做处理，用之前的逻辑，在`OsDispatch` 函数中调用新增的`IsPcbBelongRunPidContainer`函数校验要删除的PCB是否存在与当前运行进程同属与一个命名空间

`OsDispatch`函数中调用`IsPcbBelongRunPidContainer`函数判断传入的PCB是不是和运行进程处于同一个容器，当进程正常退出时，传入的PID是父pid,运行进程的container资源已释放，不能使用`IsPcbBelongRunPidContainer`判断，所以加了`pid  != OsCurrProcessGet()->parentProcessID`的条件判断。

	#ifdef LOSCFG_PID_CONTAINER
    if (pid  != OsCurrProcessGet()->parentProcessID) {
        UINT32 ret = IsPcbBelongRunPidContainer(spcb);
        if (ret != LOS_OK) {
            return -ESRCH;
        }
    }
	#endif


### 1.5 PID相关系统调用修改

获取当前运行的层级，当等于0时，PID为globalPid，按照liteos_a本身的逻辑处理，不做修改；当level>0时，将pid转换成globalPid，按照liteos_a本身的逻辑处理。

#### unshare 
 功能：传参数CLONE_NEWPID创建一个新的pidContainer，后续子进程创建用新创建的pidContainer，父进程本身的pidContainer信息不变。

 实现：当传CLONE_NEWPID时，新建一个containerBundle，将当前运行进程的pidContainer替换新建containerBundle的pidContainer以保证运行进程的信息不变并且子进程创建用新创建的containerBundle下的pidContainerForChild，修改对应计数，将新的containerBundle挂到运行PCB上，修改对应计数。

	#ifdef LOSCFG_PID_CONTAINER
    if (flags & CLONE_NEWPID) {
        DerefPidContainer(bundle->pidContainer);
        bundle->pidContainer = currProcess->containerBundle->pidContainer;
        GetPidContainer(bundle->pidContainer);
    }
	#endif
#### setns 
 功能：传参数CLONE_NEWPID时，后续子进程创建用传参fd对应的pidContainer管理，父进程本身的pidContainer信息不变。

 实现：当传CLONE_NEWPID时，拷贝一个运行进程的containerBundle，将传参fd对应的pidContainer替换拷贝containerBundle的pidContainerForChild，以保证运行进程的信息不变并且子进程创建用传参fd对应的pidContainer，修改对应计数，将新的containerBundle挂到运行PCB上，修改对应计数。

    DerefPidContainer(bundle->pidContainerforChild);
    bundle->pidContainerforChild = targetPidContainer;
    GetPidContainer(bundle->pidContainerforChild);
#### ohoscapget 
修改`SysCapGet`函数，将传入的pid转换为全局globalPid处理，找不到报错ESRCH。

#### sched_rr_get_interval
修改`SysSchedRRGetInterval`函数，将传入的pid转换为全局globalPid处理，找不到报错ESRCH。

#### sched_getaffinity/sched_setaffinity
`sched_getaffinity/sched_setaffinity`都调用了`SchedAffinityParameterPreprocess`函数，
修改`SchedAffinityParameterPreprocess`函数，将传入的pid转换为全局globalPid处理，找不到报错ESRCH。

#### sched_getscheduler
修改`SysSchedGetScheduler`函数，将传入的pid转换为全局globalPid处理，找不到报错ESRCH。

#### sched_setscheduler
修改`SysSchedSetScheduler`函数，将传入的pid转换为全局globalPid处理，找不到报错ESRCH。

#### sched_getparam
修改 `SysSchedGetParam`函数，将传入的pid转换为全局globalPid处理，找不到报错ESRCH。

#### sched_setparam
修改`SysSetProcessPriority`函数，将传入的pid转换为全局globalPid处理，找不到报错ESRCH。

#### setpgid
修改`SysSetProcessGroupID`函数，将传入的pid转换为全局globalPid处理，找不到报错ESRCH。

####  getpgrp
修改`SysGetProcessGroupID`函数，将传入的pid转换为全局globalPid处理，找不到报错ESRCH。

#### waitid
修改`SysWaitid`函数，当flag为P_PID时，将传入的pid转换为全局globalPid处理，找不到报错ESRCH。

#### getpriority
修改`SysGetProcessPriority`函数，当flag为LOS_PRIO_PROCESS时，将传入的pid转换为全局globalPid处理，找不到报错ESRCH。

#### setpriority
修改`SysSetProcessPriority`函数，当flag为LOS_PRIO_PROCESS时，将传入的pid转换为全局globalPid处理，找不到报错ESRCH。

#### waitpid
修改`LOS_Wait`函数，当flag为P_PID时，将传入的pid转换为全局globalPid处理，找不到报错ESRCH。


### 1.6 PID相关shell命令

#### vmm
修改`OsDumpAspace`函数和`OsDoDumpVm`函数，将传入的pid转换为全局globalPid处理，打印信息显示的pid由全局globalPid转换成对应pid命名空间的pid，并进行判断该PCB是否和当前运行的pcb处于同一个PidContainer，不是同个PidContainer不展示其信息。

#### cpup
修改`OsShellCmdCpup`函数和`OsCmdCpupOperateTwoParam`函数，将传入的pid转换为全局globalPid处理，打印信息显示的pid由全局globalPid转换成对应pid命名空间的pid。

#### v2p
修改`OsShellCmdV2P`函数，将传入的pid转换为全局globalPid处理，打印信息显示的pid由全局globalPid转换成对应pid命名空间的pid。

#### kill
修改`OsShellCmdKill`函数 ，将传入的pid转换为全局globalPid处理， 内核逻辑同1.4。

#### task
修改`AllTaskInfoDataShow`函数和`AllProcessDataShow`函数，在之前遍历全局PCB数组块的基础上增加了判断该PCB是否和当前运行的pcb处于同一个PidContainer，不是同个PidContainer不展示其信息。


## 规格说明
 - PidContainer容器最多3个层级：level0、level1、level2。

 - PidContainer容器最大数为系统进程最大数（采用宏配置）。

## 编译开关

 - 本模块功能特性采用编译宏 "LOSCFG_PID_CONTAINER" 进行开关控制，y打开，n关闭，默认关闭。
   
		config PID_CONTAINER
 		   bool "Enable PID container"
 		   default n
 
 - 本模块测试用例采用编译宏 "LOSCFG_USER_TEST_PID_CONTAINER" 进行开关控制，enable打开，unable关闭，默认关闭。
 