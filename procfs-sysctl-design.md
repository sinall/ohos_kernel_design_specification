# 用户态信息配置设计

# Kernel-5.10 调查

## 工作原理

以`/proc/sys/fs/file-max`为例，内核初始化时会创建该伪文件，并且将该伪文件与内核参数`files_stat.max_files`绑定。用户在配置该文件即会修改对应的内核参数。由于`alloc_empty_file`等函数使用了该内核参数，从而配置该文件就达到了配置内核的作用。

## 数据结构设计
![sysctl 数据结构](images/sysctl-data-structure.png)

## 流程设计
![sysctl 流程](images/sysctl-flow.png)

# LiteOS-A 设计


## 工作原理

内核初始化proc/sys/user目录，生成文件max_uts_container，max_user_container，max_mnt_container，max_pid_container，max_net_container，并且将伪文件与内核参数绑定。用户配置伪文件即会修改对应的内核参数。当前命名空间数量如果小于上限，则创建新的命名空间，否则返回失败。

## 数据结构设计
![sysctl 数据结构](images/liteos_a-sysctl-data-structure.png)

## 流程设计
![sysctl 数据结构](images/liteos_a-sysctl-flow.png)

# 规格说明

用户态配置内核参数时，支持每一种容器最大数为系统进程最大数（采用宏配置）

# 编译开关

不涉及

# 附录：整理已有配置头文件说明

## [vfsconfig.h 整理](procfs-vfsconfig-excel.md)
## [lwipopts.h 整理](procfs-lwipopts-excel.md)



