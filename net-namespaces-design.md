# 网络命名空间设计

# Kernel-5.10 调查

## 简介

网络命名空间为进程的网络环境提供了隔离手段。
创建一个网络命名空间的构成要素：
1. 路由表
2. iptable
3. 网卡设备
4. socket
5. 内核参数

## 网络命名空间（net namespace）创建

**原理介绍**

操作namespace的相关系统调用有3个：

 - clone(): 创建一个新的进程并把他放到新的namespace中。

 - setns(): 将当前进程加入到已有的namespace中。

 - unshare(): 使当前进程退出指定类型的namespace，并加入到新创建的namespace（相当于创建并加入新的namespace）。


**数据结构**

~~~

网络命名空间对应结构体 struct net ，定义了回环设备，loopback_dev、路由表和ipfilter等等。

#include <linux/seq_file_net.h>
struct net {
	refcount_t		refcount;
	struct net_device       *loopback_dev;          /* The loopback */
	struct netns_ipv4	ipv4;
};

核心数据结构：struct netns_ipv4,其中定义了：路由表、ipfilter、内核参数。
struct netns_ipv4 {
    //路由表
    struct fib_table __rcu  *fib_main;
    struct fib_table __rcu  *fib_default;

    //IP表（IP filter）
	struct xt_table		*iptable_filter;
	struct xt_table		*iptable_mangle;
	struct xt_table		*iptable_raw;
	struct xt_table		*arptable_filter;

    struct local_ports ip_local_ports;

    //内核参数
    int sysctl_tcp_wmem[3];
    int sysctl_tcp_rmem[3];

};
~~~

## 创建首个网络命名空间

系统init进程，会创建默认的网络空间，对应的变量为：

~~~
/* Init's network namespace */
extern struct net init_net;
~~~

## 各进程创建网络命名空间

方法是：

~~~
new_nsp->net_ns = copy_net_ns(flags, user_ns, tsk->nsproxy->net_ns);
~~~

# LiteOS-A 设计

## 1. 工作原理

网络容器对TCP/IP协议栈和网卡资源进行隔离，以达到隔离目的。

 - 传输层隔离：对端口进行隔离，进程使用的是容器内端口而不是主机端口。
 - IP层隔离：对IP资源进行隔离，每个容器都有属于自己的IP，不同容器之间IP不能相同。
 - 网络设备隔离：对网卡进行隔离，每个容器都有属于自己的网卡。

## 2. 数据结构设计

修改进程PCB结构体，在网络命名空间中添加网络设备等。

采用编译宏 LOSCFG_NET_CONTAINER 控制本功能，需要关闭网络命名空间特性时，关闭编译宏则可以恢复原来代码逻辑中原有的3个网络管理全局变量。

~~~
struct NetContainer {
    struct netif *netifList;
    struct netif *netifDefault;
    struct netif *loopNetif;
} NetContainer;
~~~

## 3. 流程设计

### 3.1 netif初始化流程

![netif初始化流程](images/net_container_flow2.png)

### 3.2 数据收发流程

![数据收发流程](images/netif_input_output.png)

### 3.3 网络命名空间流程

![网络命名空间流程](images/net_container_flow.png)

## 4. 规格说明

 - 网络容器最大数为系统进程最大数（采用宏配置）；

## 5. 编译开关

 - 本模块功能特性采用 编译宏 "LOSCFG_NET_CONTAINER" 进行开关控制，y打开，n关闭，默认关闭。

~~~
	config NET_CONTAINER
		bool "Enable net container"
		default y
~~~

 - 本模块测试用例采用编译宏"LOSCFG_USER_TEST_NET_CONTAINER"进行开关控制，enable打开，disable关闭，默认关闭。
