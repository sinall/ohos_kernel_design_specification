# 主机命名空间设计

# Kernel-5.10 调查

## 工作原理
`sethostname()`/`setdomainname()` 系统调用设置各个`uts namespace`下的`hostname`/`domainname`，从而达到隔离的目的。

## 数据结构设计

	struct uts_namespace {
		struct kref kref;
		struct new_utsname name;
		struct user_namespace *user_ns;
		struct ucounts *ucounts;
		struct ns_common ns;
	} __randomize_layout;
	extern struct uts_namespace init_uts_ns;

## 流程设计
![主机命名空间流程](images/uts-namespaces-flow.png)

# LiteOS-A 设计

## 工作原理
`sethostname()`/`setdomainname()` 系统调用设置各个`uts namespace`下的`hostname`/`domainname`，从而达到隔离的目的。

## 数据结构设计
![主机命名空间流程](images/liteos-uts-containers-data-structure.png)

## 流程设计
![主机命名空间流程](images/liteos-uts-containers-flow.png)

### 资源销毁
“主机命名空间”被“ContainerBundle”所引用。

1. 新创建的“主机命名空间”引用计数为**1**
2. 有新的`ContainerBundle`引用它（比如`clone`时不带`CLONE_NEWUTS`时），则引用计数**\+1**
3. 进程转移到别的“主机命名空间”时，原“主机命名空间”引用计数**\-1**
4. 进程销毁时，进程引用的`ContainerBundle`引用计数**\-1**，该`ContainerBundle`引用**归0**时自身销毁，其引用的所有命名空间引用计数**\-1**

通过增减`UtsContainer.base.refCount`引用计数控制，也就是说，由引用它的涉及到的修改及检查点包括：

1. `clone`
2. `unshare`
3. `setns`
4. `fork`等进程创建函数
5. 进程退出

## 规格说明

 - UTSContainer容器最大数为系统进程最大数（采用宏配置）。

## 编译开关

 - 本模块功能特性采用编译宏 "LOSCFG_UTS_CONTAINER" 进行开关控制，y打开，n关闭，默认关闭。

        config UTS_CONTAINER
            bool "Enable UTS container"
            default n
 
 - 本模块测试用例采用编译宏 "LOSCFG_USER_TEST_UTS_CONTAINER" 进行开关控制，enable打开，disable关闭，默认关闭。

 ![主机命名宏控制](images/liteos-uts-container-macro.png)
