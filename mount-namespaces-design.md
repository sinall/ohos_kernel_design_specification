# 文件系统命名空间设计

# Kernel-5.10调查

## 工作原理

子系统此前的全局属性现在封装到命名空间中，每个进程关联到一个选定的命名空间。每个可以感知命名空间的内核子系统都必须提供一个数据结构，将所有通过命名空间形式提供的对象集中起来。struct nsproxy用于汇集指向特定于子系统的命名空间包装器的指针：

    struct nsproxy {
        atomic_t count;
        struct mnt_namespace *mnt_ns;
    };

已经装载的文件系统的视图，在struct mnt_namespace中给出。\
创建新进程时使用clone建立一个新的命名空间，提供控制该行为的适当的标志。\
每个进程都关联到自身的命名空间视图：

    struct task_struct {
        /* 命名空间 */
        struct nsproxy *nsproxy；
    };

init_nsproxy定义了初始的全局命名空间，其中维护了指向各子系统初始的命名空间对象的指针：

    struct nsproxy init_nsproxy=INIT_NSPROXY(init_nsproxy);
    #define INIT_NSPROXY(nsproxy) {
        .count=ATOMIC_INIT(1),
        .mnt_ns=NULL,
    }

**多设备的层次化（mount/vfsmount）**

使用父子树的形式来构造，父设备树中的一个文件夹 struct dentry 可以充当子设备树的挂载点 mountpoint。\
为了组成复杂的父子树，系统定义了一个 struct mount 结构来负责对一个设备内子树的引用。

.mount/vfsmount
![mountpoint](images/mount_vfsmount.png)

.mount tree
![mountpoint](images/mount_tree.png)

通过一个 struct mount 结构负责引用一颗子设备树，把这颗子设备树挂载到父设备树的其中一个 dentry 节点上。
如果 dentry 成为了挂载点 mountpoint，会给其标识成 DCACHE_MOUNTED。查找路径的时候会判断 dentry 的 DCACHE_MOUNTED 标志，一旦置位就变成了 mountpoint，挂载点文件夹下原有的内容就不能访问了，转而访问子设备树根节点下的内容。
struct mount 结构之间也构成了树形结构。


## 数据结构设计

文件命名空间的结构设计包含于以下路径中：

    /kernel/linux/linux-5.10/include/linux/sched.h

    /kernel/linux/linux-5.10/include/linux/nsproxy.h

    /kernel/linux/linux-5.10/fs/mount.h

    /kernel/linux/linux-5.10/include/linux/ns_common.h

![task_struct](images/task_struct.png)

    struct nsproxy {
		atomic_t count;
		struct mnt_namespace *mnt_ns；
    };
    struct mnt_namespace {
		atomic_t count;
		struct ns_common ns;
		struct mount *root;
		struct list_head list;
		spinlock_t ns_lock;
		struct user_namespace *user_ns;
		struct ucounts *ucounts;
		u64 seq;
		wait_queue_head_t poll;
		u64 event;
		unsigned int mounts;
		unsigned int pending_mounts;
    } __randomize_layout;
    struct mount {
		struct hlist_node mnt_hash;
		struct mount *mnt_parent;
		struct dentry *mnt_mountpoint;
		struct vfsmount mnt;
		union {
			struct rcu_head mnt_rcu;
			struct llist_node mnt_llist;
		};
		struct list_head mnt_mounts;
		struct list_head mnt_child;
		struct mount *mnt_master;
		struct mnt_namespace *mnt_ns;
		struct mountpoint *mnt_mp;
		union {
			struct hlist_node mnt_mp_list;
			struct hlist_node mnt_umount;
		};
		int mnt_id;
		int mnt_group_id;
		int mnt_expiry_mark;
		struct hlist_head mnt_pins;
		struct hlist_head mnt_stuck_children;
	} __randomize_layout;
    struct dentry {
		unsigned int d_flags;
		seqcount_spinlock_t d_seq;
		struct hlist_bl_node d_hash;
		struct dentry *d_parent;
		struct qstr d_name;
		struct inode *d_inode;
		struct lockref d_lockref;
		const struct dentry_operations *d_op;
		struct super_block *d_sb;
		unsigned long d_time;
		void *d_fsdata;
		union {
			struct list_head d_lru;
			wait_queue_head_t *d_wait;
		};
		struct list_head d_child;
		struct list_head d_subdirs;
		union {
			struct hlist_node d_alias;
			struct hlist_bl_node d_in_lookup_hash;
			struct rcu_head d_rcu;
		} d_u;
    } __randomize_layout;
    struct inode {
		umode_t i_mode;
		unsigned short i_opflags;
		kuid_t i_uid;
		kgid_t i_gid;
		unsigned int i_flags;
		const struct inode_operations *i_op;
		struct super_block *i_sb;
		struct address_space *i_mapping;
		struct hlist_node i_hash;
		struct list_head i_io_list;
		union {
			const struct file_operations *i_fop;
			void (*free_inode)(struct inode *);
		};
    };

![mnt_namespace](images/mnt_namespace.png)
![mnt_namespace2](images/mnt_namespace2.png)

## 流程设计

### clone
调用流程：

![clone_posix](images/clone_posix.png)

### unshare
调用流程：

![unshare流程](images/unshare-flow.png)

### setns

调用流程：
![sentns_posix](images/setns-posix-1.png)

setns数据结构说明：

![setns_struct](images/sentns-1.png)

# LiteOS_A设计

## 工作原理

- 核心原理：用于保存文件挂载信息的链表mountList，原本保存在g_mountList中供全局访问，现在mountList修改为只在struct MntContainer中保存，从而实现在各自进程中的容器内可见，达到相互隔离的效果。

- 命名空间下挂载数据隔离原理：
	1. clone(CLONE_NEWNS)，创建新进程时使用clone函数传入CLONE_NEWNS标志，这样clone出来的子进程将拥有新的命名空间MntContainer，在新的mntContainer下创建新的mountList链表地址空间，并将父进程的mountList数据浅拷贝过来，对挂载源对应的vnode节点的mntCount进行引用计数加1，这样新的命名空间就可以独享其中的mountList数据，在子进程中mount umount操作都只会修改自己命名空间下的mountList，从而达到文件挂载信息的隔离效果。

	2. clone(不带CLONE_NEWNS)，如果不提供CLONE_NEWNS标志，则共享父进程MntContainer以及其中保存的mountList数据，只需维护好MntContainer的引用计数count即可达到共享的效果。

- 详细的流程设计及细节处理请参看流程设计章节


## 数据结构设计
struct ContainerBundle用于汇集指向特定于子系统的命名空间包装器的指针，和其他命名空间一样，文件挂载命名空间MntContainer同样也是保存在ContainerBundle中：


    struct ContainerBundle {
        atomic_t count;
        struct MntContainer *mntContainer;
    };

文件挂载命名空间MntContainer：

    typedef struct MntContainer {
		int count;
		struct ContainerBase containerBase;
		LIST_HEAD *mountList;
    };

MntContainer数据结构说明：
- count: 在没有CLONE_NEWNS的情况下需要引用同一个mntContainer，因此需要count来记录引用计数

- containerBase: ContainerBase是所有命名空间都需要使用的结构体，用于实现setns功能，详情参看setns相关设计文档

- *mountList: 每个命名空间下都需要保存一份mountList数据用于达到隔离效果

所有新挂载节点的全局缓存：

    LIST_HEAD *g_mountCache;


为什么需要设计一个mount节点的全局缓存：

- 因为各进程在各自命名空间下无法感知其他进程是否已经挂载过某挂载源，没有缓存的情况下，同一个挂载源将无法被多个命名空间下使用，而对用户来说。

- 例证：假设进程1挂载了挂载源1，而进程2与进程1挂载命名空间隔离了，并且没有挂载挂载源1，此时如果没有mount缓存，进程2想要挂载挂载源1将无法完成操作，进而达不到隔离的效果。因此在全局需要保存一份所有mount节点的缓存数据，用于各进程复用已经被使用的挂载源，新的命名空间下想要挂载已有的挂载源，只需将挂载源对应缓存的vnodeCovered的mntCount计数加一即可，从而达到各自命名空间下相互隔离的效果。

虚拟文件节点vnode：

    struct Vnode {
    enum VnodeType type;                /* vnode type */
    int useCount;                       /* ref count of users */
    uint32_t hash;                      /* vnode hash */
    uint uid;                           /* uid for dac */
    uint gid;                           /* gid for dac */
    mode_t mode;                        /* mode for dac */
    LIST_HEAD parentPathCaches;         /* pathCaches point to parents */
    LIST_HEAD childPathCaches;          /* pathCaches point to children */
    struct Vnode *parent;               /* parent vnode */
    struct VnodeOps *vop;               /* vnode operations */
    struct file_operations_vfs *fop;    /* file operations */
    void *data;                         /* private data */
    uint32_t flag;                      /* vnode flag */
    LIST_ENTRY hashEntry;               /* list entry for bucket in hash table */
    LIST_ENTRY actFreeEntry;            /* vnode active/free list entry */
    struct Mount *originMount;          /* fs info about this vnode */
    struct Mount *newMount;             /* fs info about who mount on this vnode */
    char *filePath;                     /* file path of the vnode */
    struct page_mapping mapping;        /* page mapping of the vnode */
	int mntCount;                       /* ref count of mounts */
	};

为什么需要添加mntCount：为了实现同一个挂载源被多个命名空间共享，需要在Vnode添加了对挂载节点的引用计数mntCount

## 数据结构关系图
![mnt_container_struct](images/MntContainer().drawio.png)

## 流程设计

![mnt_container](images/MntContainer流程.png)

流程关键点详解：

1. GetMountList函数原先是全局共享一个g_mountList，现改为从各自命名空间下获取mountList。

2. 在创建新的MntContainer时，需要开辟新的newMountList内存空间，再将老的oldMountList中的数据赋值给newMountList，无需深拷贝vnode节点，最后将vnodeVovered的mntCount计数加1即可。

3. 在mount的时候，原流程只需要判断vnode节点的flag是否已挂载即可，现在需要增加判断自己的命名空间中是否是否包含该挂载节点。如果已经挂载则直接返回已挂载。\
如果没有挂载的情况下，还需要判断缓存中是否存在有这个指定的挂载源source。如果缓存中包含这个挂载源，则只需从缓存中拿出这个挂载源对应的mount数据，赋值给新建mount节点并添加到自己命名空间下的mountList中，再将对应的vnodeVovered的mntCount计数加1即可。
如果缓存中没有找到指定的挂载源source，走完原来的挂载流程即可。

4. 在umount的时候，原流程只需要判断vnode节点的flag是否已挂载即可，现在需要增加判断自己的命名空间中是否是否包含该挂载节点。如果没有挂载则直接返回失败。\
如果有挂载则需要判断vnode的mntCount的引用计数是否是最后一个，如果不是则只需要将引用计数减1，则将对应的mnt节点从自己命名空间下的mountList中删掉即可，无需真正的去umount，因为还有其他进程在使用该挂载源。\
如果引用计数是最后一个，那么需要将其对应的mnt节点从全局缓存中移除，同时走正常的umount流程。\
最后还需要从缓存中遍历判断下vnodeBecovered是否还有挂载，没有任何挂载该节点的情况下才可以将其flag置为未挂载。

5. open、access等涉及文件查找的操作函数调用中，需要对ConvertVnodeIfMounted进行一定改造，需要从各自命名空间下查找到对应挂载vnodeCovered

## 依赖关系用例图

![mnt_container](images/mount依赖关系用例.drawio.png)

该用例图描述了如下用户操作：
1. 在pcb102中mount 192.168.1.13/mnt1 /test1 nsf

2. 然后从pcb102 clone(CLONE_NEWNS)出pcb103

3. 最后在pcb103中mount 192.168.1.13/mnt2 /test2 nsf

mount和vnode依赖关系解析：
- mount.vnodeBeCovered对应被挂载点 /testx 的vnode

- mount.vnodeCovered对应挂载源 192.168.1.x/mntx 的vnode

- vnodeBeCovered.newMount对应mount节点，现在已不再使用（原来是用newMount来查找挂载文件，现改为从命名空间中查找mount，从而达到不同命名空间下隔离mount列表的效果）

- vnodeCovered.originMount对应mount节点

- 在对同一挂载源共同引用时，需要对vnodeCovered的mntCount进行引用计数加1，如果mount1卸载了，只需mntCount计数减一即可

- g_cacheMount中全局保存了所有两个实际的挂载点mount和mount2

## 规格说明
 - MntContainer容器最大数为系统进程最大数（采用宏配置）。

## 编译开关

 - 本模块功能特性采用编译宏 "LOSCFG_MNT_CONTAINER" 进行开关控制，y打开，n关闭，默认关闭。

		config MNT_CONTAINER
 		   bool "Enable MNT container"
 		   default n

 - 本模块测试用例采用编译宏 "LOSCFG_USER_TEST_MNT_CONTAINER" 进行开关控制，enable打开，disable关闭，默认关闭。